<?php 

/* 
* Template Name: Promotion
*/

get_header();
global $post;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
$args = [
    'posts_per_page'    => '-1',
    'post_type'         => ['promotion'],
    'post_parent'       => 0,
    'order'             => 'DESC',
    'suppress_filters'  => false,
];
$promotions = new \WP_Query( $args ); 
wp_reset_postdata();
?>

<!-- Page Sub-Header -->
<?php include get_template_directory() . '/templates/partials/headline.php'; ?>
<!--/ Page sub-header -->


<!-- Photo gallery section with custom paddings -->
<section class="hg_section pt-80 pb-100">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- TItle element -->
				<div class="kl-title-block text-center tbk-icon-pos--before-title">
					<!-- Title -->
					<h3 class="tbk__title kl-font-alt fs-l fw-semibold dark-gray">
						EXACTLY! ONE OF THE MOST COMPLETE TEMPLATE
					</h3>
					<!--/ Title -->

					<!-- Sub-title with thin weight -->
					<h4 class="tbk__subtitle fw-thin">
						We always had this statement and we're keeping our promise. Beside a powerful yet easy to use elements, Kallyas has packed inside lots of sweet features that wait to be discovered.
					</h4>
					<!--/ Sub-title -->
				</div>
				<!--/ TItle element custom with top icon -->

				<!-- Invisible separator 20px -->
				<div class="clearfix" style="height: 20px;"></div>

				<!-- Grid Photo Gallery square ratio element container -->
				<div class="gridPhotoGallery-container">
					<div class="gridPhotoGallery mfp-gallery misc gridPhotoGallery--ratio-square gridPhotoGallery--cols-4" data-cols="4">
						<!-- Grid photo gallery size helper -->
						<div class="gridPhotoGallery__item gridPhotoGallery__item--sizer">
						</div>
						<!--/ Grid photo gallery size helper -->

						<!-- Item #1 width style 2 & height style 2 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w2">
							<!-- Pop-up image + Link & Background image -->
							<a title="Girl Smiling" class="gridPhotoGalleryItem--h2 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg1.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg1.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #1 width style 2 & height style 2 -->

						<!-- Item #2 width & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w1">
							<!-- Pop-up image + Link & Background image -->
							<a title="Morning Coffee" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg2.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg2.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #2 width & height style 1 -->

						<!-- Item #3 width & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w1">
							<!-- Pop-up image + Link & Background image -->
							<a title="Stone Arch" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg4.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg4.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #3 width & height style 1 -->

						<!-- Item #4 width style 2 & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w2">
							<!-- Pop-up image + Link & Background image -->
							<a title="A bunch of grapes" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg5.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg5.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #4 width style 2 & height style 1 -->

						<!-- Item #4 width style 1 & height style 2 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w1">
							<!-- Pop-up image + Link & Background image -->
							<a title="" class="gridPhotoGalleryItem--h2 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg6.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image:url(<?=get_template_directory_uri();?>/assets/images/gpg6.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #4 width style 1 & height style 2 -->

						<!-- Item #5 width style 2 & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w2">
							<!-- Pop-up image + Link & Background image -->
							<a title="" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg7.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg7.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #5 width style 2 & height style 1 -->

						<!-- Item #6 width style 1 & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w1">
							<!-- Pop-up image + Link & Background image -->
							<a title="" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg8.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg8.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #6 width style 1 & height style 1 -->

						<!-- Item #7 width style 1 & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w1">
							<!-- Pop-up image + Link & Background image -->
							<a title="" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg10.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg10.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!--/ Item #7 width style 1 & height style 1 -->

						<!-- Item #8 width style 2 & height style 1 -->
						<div class="gridPhotoGallery__item gridPhotoGalleryItem--w2">
							<!-- Pop-up image + Link & Background image -->
							<a title="" class="gridPhotoGalleryItem--h1 gridPhotoGallery__link hoverBorder" data-lightbox="mfp" data-mfp="image" href="<?=get_template_directory_uri();?>/assets/images/gpg9.jpg">
								<!-- Background image -->
								<div class="gridPhotoGallery__img" style="background-image: url(<?=get_template_directory_uri();?>/assets/images/gpg9.jpg)">
								</div>
								<!--/ Background image -->

								<!-- Icon -->
								<i class="kl-icon fas fa-search circled-icon ci-large"></i>
								<!--/ Icon -->
							</a>
							<!--/ Pop-up image + Link & Background image -->
						</div>
						<!-- Item #8 width style 2 & height style 1 -->
					</div>
					<!--/ .gridPhotoGallery ratio-square cols-4 -->
				</div>
				<!--/ Grid Photo Gallery square ratio element container -->
			</div>
			<!--/ col-sm-12 col-md-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Photo gallery section with custom paddings -->



<!-- JS FILES // Loaded on this page -->
<!-- Required script for sorting (masonry) elements - Isotope filter -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/plugins/jquery.isotope.min.js"></script>

<!-- Required js trigger for Portfolio sortable element -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/trigger/kl-portfolio-sortable.js"></script>

<!-- Custom Kallyas JS codes -->
<!-- <script type="text/javascript" src="js/kl-scripts.js"></script> -->


<?php get_footer(); ?>