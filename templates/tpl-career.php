<?php 

/* 
* Template Name: Career
*/

get_header();
global $post;
?>


<!-- Page sub-header + Bottom mask style 3 -->
<?php include get_template_directory() . '/templates/partials/headline.php'; ?>
<!-- Page sub-header + Bottom mask style 3 -->

<!-- Content page section with custom paddings -->
<section class="hg_section pt-80">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- Page title & Subtitle-->
				<div class="kl-title-block tbk--left tbk-symbol--line tbk-icon-pos--after-title">
					<!-- Title with custom font, size and color -->
					<h2 class="tbk__title kl-font-alt fs-34 black">
						<strong><?=__("WE'RE HIRING", 'gg') ?>!</strong>
					</h2>

					<!-- Title bottom line symbol -->
					<div class="tbk__symbol">
						<span></span>
					</div>
					<!--/ Title bottom line symbol -->

					<!-- Sub-title with custom font size and weight -->
					<?=apply_filters('the_content', $post -> post_content);?>
				</div>
				<!--/ Page title & Subtitle-->	
			</div>
			<!--/ col-sm-12 col-md-12 -->



			<div class="col-sm-12 col-md-12 col-lg-8 mb-md-50">
				<!-- Accordions element style 3 -->
				<div class="hg_accordion_element style3">
					<!-- Accordions wrapper -->
					<div class="th-accordion">
						<!-- Acc group #7 -->
						<?php foreach ($post -> carrer_benef as $key => $vac): ?>
							<div class="acc-group">
								<!-- Title group button acc #7 -->
								<div id="headingSeven">
									<a data-toggle="collapse" data-target="#acc<?=$key;?>" class="at collapsed" aria-expanded="true" aria-controls="acc<?=$key;?>">
										<?=$vac['car2_title'];?>
										<span class="acc-icon"></span>
									</a>
								</div>
								<!--/ Title group button acc #7 -->

								<!-- Acc #7 -->
								<div id="acc<?=$key;?>" class="collapse" aria-labelledby="headingSeven" data-parent=".style3">
									<!-- Content -->
									<div class="content">
										<div class="row">
											<div class="col-sm-12">
												<h4>
													<?=__('Description', 'gg'); ?>
												</h4>

												<p>
													<?=wpautop($vac['car2_desc']);?>
												</p>
											</div>
											<!--/ col-sm-6 -->

											<div class="col-sm-12">
												<h4>
													<?=__('Technical Skills', 'gg'); ?>
												</h4>

												<ul class="list-style2">
													<?php foreach ($vac['car2_req'] as $key => $req): ?>
														<li>
															<?php echo $req; ?>
														</li>
													<?php endforeach ?>
												</ul>
											</div>
											<!--/ col-sm-6 -->
										</div>
										<!--/ row -->
									</div>
									<!--/ Content -->
								</div>
								<!--/ Acc #7 -->
							</div>
						<?php endforeach ?>
						<!--/ Acc group #7 -->
					</div>
					<!--/ Accordions wrapper -->
				</div>
				<!--/ Accordions element style 3 -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-8 mb-md-50 -->

			<div class="col-sm-12 col-md-12 col-lg-4">
				<!-- Media container - set custom height -->
				<div class="media-container" style="height: 560px;">
					<!-- kl-bg-source -->
					<div class="kl-bg-source">
						<!-- video container -->
						<div class="kl-video-container kl-bg-source__video">
							<!-- video wrapper -->
							<div class="kl-video-wrapper video-grid-overlay">
								<!-- Youtube Source -->
								<div class="kl-video valign halign" style="width: 100%; height: 100%;" data-setup='{ 
									"position": "absolute", 
									"loop": true, 
									"autoplay": true, 
									"muted": true, 
									"youtube":"<?=$post -> youtube;?>", 
									"poster": "<?php echo get_template_directory_uri();?>/assets/images/home-office-569359_640.jpg", 
									"video_ratio": "1.7778" }'>
								</div>
							</div>
							<!--/ video-wrapper -->
						</div>
						<!--/ video container -->

						<!-- Video overlay -->
						<div class="kl-bg-source__overlay" style="background-color: rgba(53,53,53,0.8);">
						</div>
						<!--/ Video overlay -->
					</div>
					<!--/ kl-bg-source -->

					<!-- Media container video pop-up -->
					<a class="media-container__link media-container__link--btn media-container__link--style-circle " href="https://www.youtube.com/watch?v=<?=$post -> youtube;?>" data-lightbox="iframe">
						<!-- Play circle icon svg -->
						<div class="circleanim-svg">
							<svg height="108" width="108" xmlns="https://www.w3.org/2000/svg">
							<circle stroke-opacity="0.1" fill="#FFFFFF" stroke-width="5" cx="54" cy="54" r="48" class="circleanim-svg__circle-back"></circle>
							<circle stroke-width="5" fill="#FFFFFF" cx="54" cy="54" r="48" class="circleanim-svg__circle-front" transform="rotate(50 54 54) "></circle>
							<path d="M62.1556183,56.1947505 L52,62.859375 C50.6192881,63.7654672 49.5,63.1544098 49.5,61.491212 L49.5,46.508788 C49.5,44.8470803 50.6250889,44.2383396 52,45.140625 L62.1556183,51.8052495 C64.0026693,53.0173767 63.9947588,54.9878145 62.1556183,56.1947505 Z" fill="#FFFFFF"></path>
							</svg>
						</div>
						<!--/ Play circle icon svg -->
					</a>
					<!--/ Media container video pop-up -->
				</div>
				<!--/ Media container -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-4 -->

			<!-- Separator with custom margins -->
			<div class="col-sm-12">
				<div class="hg_separator mt-70"></div>
			</div>
			<!--/ col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!-- Content page section with custom paddings -->

<!-- Contact form & details section with custom paddings -->
<section id="contact" class="hg_section pt-0 pb-80">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<!-- Title element -->
				<div class="kl-title-block">
					<!-- Title with custom size & weight -->
					<h3 class="tbk__title fs-m fw-semibold">
						<?=__('LETS WORK TOGETHER', 'gg'); ?>
					</h3>

					<!-- Sub-title with custom size -->
					<h4 class="tbk__subtitle fs-normal">
						<?=__('AND MAKE THE NEXT BEST PROJECT', 'gg'); ?>
					</h4>
				</div>
				<!--/ Title element -->
			</div>
			<!--/ col-sm-12 -->

			<div class="col-sm-12 col-md-12 col-lg-9 mb-md-30">
				<!-- Contact form element -->
				<div class="contactForm">
					<form action="" method="post" class="contact_form row">
						<!-- Response container -->
						<div class="cf_response"></div>
						<!--/ Response container -->

						<div class="col-sm-6 kl-fancy-form">
							<input type="text" name="name" id="cf_name" class="form-control" placeholder="<?=__('Please enter your first name', 'gg'); ?>" value="" tabindex="1" maxlength="35" required>
							<label class="control-label">
								<?=__('FIRSTNAME', 'gg'); ?>
							</label>
						</div>

						<div class="col-sm-6 kl-fancy-form">
							<input type="text" name="lastname" id="cf_lastname" class="form-control" placeholder="<?=__('Please enter your last name', 'gg'); ?>" value="" tabindex="1" maxlength="35" required>
							<label class="control-label">
								<?=__('LASTNAME', 'gg'); ?>
							</label>
						</div>

						<div class="col-sm-12 kl-fancy-form">
							<input type="text" name="email" id="cf_email" class="form-control h5-email" placeholder="<?=__('Please enter your email', 'gg'); ?>" value="" tabindex="1" maxlength="35" required>
							<label class="control-label">
								<?=__('EMAIL', 'gg'); ?>
							</label>
						</div>

						<div class="col-sm-12 kl-fancy-form">
							<input type="text" name="subject" id="cf_subject" class="form-control" placeholder="<?=__('Please enter your subject', 'gg'); ?>" value="" tabindex="1" maxlength="35" required>
							<label class="control-label">
								<?=__('SUBJECT', 'gg'); ?>
							</label>
						</div>

						<div class="col-sm-12 kl-fancy-form">
							<textarea name="message" id="cf_message" class="form-control" cols="30" rows="10" placeholder="<?=__('Your message', 'gg'); ?>" tabindex="4" required></textarea>
							<label class="control-label">
								<?=__('MESSAGE', 'gg'); ?>
							</label>
						</div>

						<!-- Google recaptcha required site-key (change with yours => https://www.google.com/recaptcha/admin#list) -->
						<div class="g-recaptcha" data-sitekey="6Lc9txAUAAAAAD4PbKg_oKQIgsVj2-oIMaBVwTE0"></div>
						<!--/ Google recaptcha required site-key -->

						<div class="col-sm-12">
							<!-- Contact form send button -->
							<button style="font-weight: 400;" class="btn btn-fullcolor mail-send-button" type="submit">
								<?=__('Send', 'gg'); ?>
							</button>
						</div>
					</form>
				</div>
				<!--/ Contact form element -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-9 mb-md-30 -->

			<div class="col-sm-12 col-md-12 col-lg-3">
				<!-- Text box element -->
				<div class="text_box">
					<!-- Box title style 2 -->
					<h3 class="text_box-title text_box-title--style2"><?=__('CONTACT INFO', 'gg'); ?></h3>

					<!-- Sub-title -->
					<h4>შ.პ.ს. „იმპექსფარმი“</h4>

					<p>
						გიორგი სააკაძის II შესახვევი <br>
						თბილისი - 0160, საქართველო <br>
						
						<br>
						ტელ: +995 (32) 2313100
					</p>
				</div>
				<!--/ Text box element -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-3 -->
		</div>
		<!--/ .row -->
	</div>
	<!--/ .container -->
</section>
<!--/ Contact form & details section with custom paddings -->


<?php get_footer(); ?>