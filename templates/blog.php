<?php 

/* 
* Template Name: Blog
*/

get_header();
global $post;
$dataID = get_the_ID();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array (
  'post_type'              => array( 'post' ),
  'post_status'            => array( 'publish' ),
  'pagination'             => true,
  'paged'                  => $paged,
  'posts_per_page'         => '12',
  'order'                  => 'DESC',
  //'offset'                 => $paged,
);

$news = new WP_Query( $args );

$args2 = array (
  'post_type'              => array( 'post' ),
  'post_status'            => array( 'publish' ),
);

// The Query
$all_post = new WP_Query( $args2 );
$allPostCount = $all_post->post_count;
$allPostCount = ceil($allPostCount / 6);
?>

<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/ka_GE/sdk.js#xfbml=1&version=v3.2&appId=226615747674441&autoLogAppEvents=1"></script>

<!-- Latest news page content with white background color and custom paddings -->
<section class="hg_section pt-120 pb-80">
    <div class="container pt-50">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="blog-top-part">
                    <h3 class="global-title float-left">
                        <?=$post -> post_title;?>
                    </h3>

                    <?php $cats = get_categories(); ?>
                    <ul class="menu float-right">
                        <?php foreach ($cats as $key => $c): ?>
                            <li class="cat-item">
                                <a href="<?=get_category_link($c -> term_id);?>">
                                    <?php echo $c->cat_name; ?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    </ul> 
                    <div class="clearfix"></div> 
                </div>
                <div class="stg-negative-right stg-negative-left pb-40 pr-sm-0 clearfix">
                    <div class="latest_posts default-style row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="row">
                                <!--/ col-sm-12 col-md-12 col-lg-12 -->
                                <?php foreach ($news -> posts as $keynews => $n): ?>
                                    <?php 
                                        $keynews = $keynews + 1; 
                                        $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $n -> ID ), 'large' ,true );
                                        $content = strip_tags($n -> post_content);
                                        $content = mb_substr($content, 0, 200);
                                        $cat = get_the_category($n -> ID);
                                    ?>
     
                                        <div class="col-sm-12 col-md-4 col-lg-4 post">
                                            <!-- Post link wrapper -->
                                            <a href="<?=get_permalink($n -> ID);?>" class="hoverBorder plus">
                                              <!-- Border wrapper -->
                                              <span class="hoverBorderWrapper">
                                                <!-- Image -->
                                                <img style="height: 230px; object-fit: cover; border-radius: 15px;" src="<?=$bimage[0];?>" class="img-fluid" width="370" height="200" alt="" title="" />
                                                <!--/ Image -->

                                                <!-- Hover border/shadow -->
                                                <span class="theHoverBorder"></span>
                                                <!--/ Hover border/shadow -->
                                              </span>
                                              <!--/ Border wrapper -->

                                              <!-- Button -->
                                              <h6>
                                                <?=__('Read more', 'gg') ?> +
                                              </h6>
                                              <!--/ Button -->
                                            </a>
                                            <!--/ Post link wrapper -->


                                            <!-- Title with link -->
                                            <h3 class="m_title">
                                              <a style="font-style: normal;" href="<?=get_permalink($n -> ID);?>">
                                                <?=$n -> post_title;?>
                                              </a>
                                            </h3>
                                            <!--/ Title with link -->
                                          </div>
                                    
                                        <?php if ($keynews%3 == 0): ?>
                                            <div class="clearfix"></div>
                                        <?php endif ?>
                                <?php endforeach ?>
                                <!--/ col-sm-12 col-md-6 col-lg-6 -->
                            </div>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!-- end .blog-posts -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-8 -->
        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>

<?php get_footer() ?>