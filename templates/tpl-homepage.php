<?php 

/* 
* Template Name: Homepage
*/

get_header();
global $post;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
$apps = get_post_meta(182, 'pages', 1);
?>

<?php include get_template_directory() . '/templates/partials/slider.php'; ?>

<!-- Icon Boxes element Left Floated Style - section white -->
<section class="hg_section bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-10 offset-lg-1">
				<div class="row gutter-md">

					<?php foreach ($post -> h_services as $key => $service): ?>
						<div class="col-sm-12 col-md-6 col-lg-3">
							<!-- Icon box float left -->
							<div class="kl-iconbox kl-iconbox--fleft text-left">
								<div class="kl-iconbox__inner">

									<!-- /.kl-iconbox__icon-wrapper -->
									<div class="kl-iconbox__content-wrapper">
										<!-- Title -->
										<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
											<div class="kl-iconbox__icon-wrapper">
												<img src="<?=$service['cimage']; ?>" class="kl-iconbox__icon" alt="<?=$service['ctitle']; ?>">
											</div>
											<h3 class="kl-iconbox__title fs-m fw-normal gray2 bold">
												<?=$service['ctitle']; ?>
											</h3>
										</div>
										<!--/ Title -->

										<!-- Description -->
										<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
											<p class="kl-iconbox__desc fs-14 gray">
												<?=$service['ctext']; ?>
											</p>
										</div>
										<!--/ Description -->
									</div>
									<!-- /.kl-iconbox__content-wrapper -->
								</div>
								<!--/ kl-iconbox__inner -->
							</div>
							<!--/ Icon box float left -->
						</div>
					<?php endforeach ?>

				</div>
				<!--/ row gutter-md -->
			</div>
			<!--/ col-lg-10 offset-lg-1 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container-fluid -->
</section>
<!--/ Icon Boxes element Left Floated Style - section white -->

<!-- Image Boxes element - Style 4 - section with custom paddings -->
<section class="hg_section bg-white pb-80">
	<div class="container">
		<div class="row">
			<?php if (count($apps) > 0): ?>
				<?php foreach ($apps as $key => $appId): ?>
					<?php 
						$app = get_post($appId); 
						$bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $appId ), 'large' ,true );
					?>
					<div class="col-sm-12 col-md-6 col-lg-4">
						<!-- Image box element style 4 - left title -->
						<div class="box image-boxes imgboxes_style4 kl-title_style_left">
							<!-- Image box link wrapper -->
							<a href="<?=get_permalink($appId);?>" target="_self" class="imgboxes4_link imgboxes-wrapper" title="">
								<!-- Image -->
								<img src="<?=$bimage[0];?>" class="img-fluid imgbox_image cover-fit-img" alt="" title="" />
								<!--/ Image -->

								<!-- Border helper -->
								<span class="imgboxes-border-helper"></span>
								<!--/ Border helper -->

								<!-- Title -->
								<h3 class="m_title imgboxes-title">
									<?=$app -> post_title;?>
								</h3>
								<!--/ Title -->
							</a>
							<!--/ Image box link wrapper -->

							<!-- Content -->
							<p>
								<?=$app -> text;?>
							</p>
							<!--/ Content -->
						</div>
						<!--/ Image box element style 4 - center title -->
					</div>	
				<?php endforeach ?>
			<?php endif ?>
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Image Boxes element - Style 4 - section with custom paddings -->

<!-- shop section -->
<section class="hg_section home-shop-section pb-80">
	<div class="container">
		<div class="row text-center">
			<h2>მაღაზია</h2>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-4">
				<!-- Image box element style 4 - left title -->
				<div class="box image-boxes">
					<!-- Image box link wrapper -->
					<a href="<?=site_url();?>/e-პროდუქცია/?active=1" target="_self" class="imgboxes4_link imgboxes-wrapper" title="">
						<!-- Image -->
						<img src="http://vue.ge/gega/products/espace/wp-content/uploads/2020/12/echarger@2x.png" class="img-fluid imgbox_image cover-fit-img" alt="" title="" />
						<!--/ Image -->

						<!-- Border helper -->
						<span class="imgboxes-border-helper"></span>
						<!--/ Border helper -->

						<!-- Title -->
						<h3 class="m_title imgboxes-title">
							დამტენები >
						</h3>
						<!--/ Title -->
					</a>
					<!--/ Image box link wrapper -->
				</div>
				<!--/ Image box element style 4 - center title -->
			</div>	

			<div class="col-sm-12 col-md-6 col-lg-4">
				<!-- Image box element style 4 - left title -->
				<div class="box image-boxes">
					<!-- Image box link wrapper -->
					<a href="<?=site_url();?>/e-პროდუქცია/?active=2" target="_self" class="imgboxes4_link imgboxes-wrapper" title="">
						<!-- Image -->
						<img src="http://vue.ge/gega/products/espace/wp-content/uploads/2020/12/cable@2x.png" class="img-fluid imgbox_image cover-fit-img" alt="" title="" />
						<!--/ Image -->

						<!-- Border helper -->
						<span class="imgboxes-border-helper"></span>
						<!--/ Border helper -->

						<!-- Title -->
						<h3 class="m_title imgboxes-title">
							კაბელები >
						</h3>
						<!--/ Title -->
					</a>
					<!--/ Image box link wrapper -->
				</div>
				<!--/ Image box element style 4 - center title -->
			</div>	

			<div class="col-sm-12 col-md-6 col-lg-4">
				<!-- Image box element style 4 - left title -->
				<div class="box image-boxes">
					<!-- Image box link wrapper -->
					<a href="<?=site_url();?>/e-პროდუქცია/?active=3" target="_self" class="imgboxes4_link imgboxes-wrapper" title="">
						<!-- Image -->
						<img src="http://vue.ge/gega/products/espace/wp-content/uploads/2020/12/cable@2x.png" class="img-fluid imgbox_image cover-fit-img" alt="" title="" />
						<!--/ Image -->

						<!-- Border helper -->
						<span class="imgboxes-border-helper"></span>
						<!--/ Border helper -->

						<!-- Title -->
						<h3 class="m_title imgboxes-title">
							უნუ >
						</h3>
						<!--/ Title -->
					</a>
					<!--/ Image box link wrapper -->
				</div>
				<!--/ Image box element style 4 - center title -->
			</div>	
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!-- shop section ennd -->

<!-- Recent Work carousel element - section with custom paddings -->
<section class="hg_section pt-140 pb-140">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- Recent Work carousel 1 style 2 element -->
				<div class="recentwork_carousel recentwork_carousel--2 clearfix">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-4">
							<!-- Left side -->
							<div class="recentwork_carousel__left mb-md-50">
								<!-- Title -->
								<h3 class="recentwork_carousel__title m_title bold">
									ბოლოს დამატებული
								</h3>
								<!--/ Title -->

								<!-- Description -->
								<p class="recentwork_carousel__desc">
									Lorem Ipsum საბეჭდი და ტიპოგრაფიული ინდუსტრიის უშინაარსო ტექსტია. იგი სტანდარტად 1500-იანი წლებიდან იქცა, როდესაც უცნობმა მბეჭდავმა ამწყობ დაზგაზე წიგნის საცდელი ეგზემპლარი 
								</p>

								<!-- View all button -->
								<a href="<?=site_url();?>/blog" class="btn btn-fullcolor">
									ყველას ნახვა
								</a>
								<!--/ View all button -->

								<!-- Slick navigation -->
								<div class="hgSlickNav recentwork_carousel__controls"></div>
							</div>
							<!--/ Left side - .recentwork_carousel__left -->
						</div>
						<!--/ col-sm-12 col-md-12 col-lg-4 -->

						<div class="col-sm-12 col-md-12 col-lg-8">
							<!-- Recent Work - carousel wrapper -->
							<div class="recentwork_carousel__crsl-wrapper">
								<div class="recent_works1 recentwork_carousel__crsl js-slick" data-slick='{
									"infinite": true,
									"slidesToShow": 3,
									"slidesToScroll": 1,
									"swipeToSlide": true,
									"autoplay": true,
									"autoplaySpeed": 5000,
									"speed": 500,
									"cssEase": "ease-in-out",
									"appendArrows": ".recentwork_carousel--2 .hgSlickNav",		
									"arrows": true,
									"responsive": [
										{
											"breakpoint": 1199,
											"settings": {
												"slidesToShow": 2
											}
										},
										{
											"breakpoint": 767,
											"settings": {
												"slidesToShow": 2
											}
										},
										{
											"breakpoint": 480,
											"settings": {
												"slidesToShow": 1
											}
										}
									]
								}'>

									<?php 
										$args = array (
										  'post_type'              => array( 'post' ),
										  'post_status'            => array( 'publish' ),
										  'posts_per_page'         => '3',
										  'order'                  => 'DESC',
										  //'offset'                 => $paged,
										);

										$news = new WP_Query( $args );
									?>
									<!-- Item #1 -->
									<?php foreach ($news -> posts as $key => $p): ?>
										<?php $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $p -> ID ), 'large' ,true ); ?>
										<div class="recent-work_carousel-item">
											<!-- Portfolio link container -->
											<a href="<?=get_permalink($p -> ID);?>" class="recentwork_carousel__link">
												<!-- Hover container -->
												<div class="hover recentwork_carousel__hover">
													<!-- Item image with custom height -->
													<div class="carousel-item--height300">
														<img src="<?=$bimage[0];?>" class="recentwork_carousel__img cover-fit-img" alt="" title="<?=$p -> post_title;?>" />
													</div>
													<!--/ Item image with custom height -->

													<!-- Hover shadow overlay -->
													<span class="hov recentwork_carousel__hov"></span>
													<!--/ Hover shadow overlay -->
												</div>
												<!--/ Hover container -->

												<!-- Content details -->
												<div class="details recentwork_carousel__details">
													<!-- Tags/Category -->
													<span class="recentwork_carousel__cat">
														სრულად
													</span>
													<!--/ Tags/Category -->

													<!-- Title -->
													<h4 class="recentwork_carousel__crsl-title">
														<?=$p -> post_title;?>
													</h4>
													<!--/ Title -->
												</div>
												<!--/ Content details -->
											</a>
											<!--/ Portfolio link container -->
										</div>
										<!--/ Item #1 -->
									<?php endforeach ?>
								</div>
								<!--/ .recent_works1 .recentwork_carousel__crsl .js-slick -->
							</div>
							<!--/  Recent Work - carousel wrapper - .recentwork_carousel__crsl-wrapper -->
						</div>
						<!--/ col-sm-12 col-md-12 col-lg-8 -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Recent Work carousel 1 style 2 element -->
			</div>
			<!--/ col-sm-12 col-md-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container-fluid -->
</section>
<!--/ Recent Work carousel element - section with custom paddings -->

<!-- Media Container - Border Animate Style 2 section with background white color -->
<section class="hg_section bg-white p-0">
	<div class="container-fluid no-pad-cols">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 col-xl-5">
				<!-- Media container style 2 element - with custom minimum height(.min-700) -->
				<div class="media-container style2 min-700">
					<!-- Background -->
					<div class="kl-bg-source">
						<!-- Background image -->
						<div class="kl-bg-source__bgimage" style="background-image: url(<?=$image[0];?>); background-repeat: no-repeat; background-attachment: scroll; background-position-x: center; background-position-y: top; background-size: cover;">
						</div>
						<!--/ Background image -->

						
					</div>
					<!--/ Background -->
				</div>
				<!--/ media-container style2 h-615 -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-12 col-xl-5 -->

			<div class="col-sm-12 col-md-12 col-lg-12 col-xl-7 d-flex">
				<div class="row hg_col_eq_last align-self-center pt-50 pb-50 ml-xl-70">
					<div class="col-sm-12 col-md-12">
						<!-- Title element -->
						<div class="kl-title-block text-left">
							<?=apply_filters('the_content', $post -> post_content);?>
						</div>
						<!--/ Title element -->
					</div>
					<!--/ col-sm-12 col-md-12 -->

				</div>
				<!--/ row hg_col_eq_last align-self-center pt-50 pb-50 ml-xl-70 -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-12 col-xl-7 d-flex -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container-fluid -->
</section>
<!--/ Media Container - Border Animate Style 2 section with background white color -->


<section class="hg_section brand-bg">
  <div class="container-fluid">
    <!-- Recent Work carousel 3 default style element -->
    <div class="recentwork_carousel recentwork_carousel_v3">
      <!-- Top container -->
      <div class="container recentwork_carousel__top-container">
        <div class="row">
          <div class="col-sm-12">
            <!-- Title -->
            <h3 class="recentwork_carousel__title m_title">
              <?=__('პარტნიორები', 'gg'); ?>
            </h3>
            <!--/ Title -->

            <!-- View all button -->
            <a href="http://vue.ge/gega/products/espace/%e1%83%9e%e1%83%90%e1%83%a0%e1%83%a2%e1%83%9c%e1%83%98%e1%83%9d%e1%83%a0%e1%83%94%e1%83%91%e1%83%98/" class="btn btn-link">
              <?=__('ყველას ნახვა', 'gg'); ?>
            </a>
            <!--/ View all button -->

            <!-- Slick navigation -->
            <div class="hgSlickNav recentwork_carousel__controls clearfix"></div>
          </div>
          <!--/ col-sm-12 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ Top container -->

      <!-- Carousel wrapper -->
      <div class="recentwork_carousel__crsl-wrapper">
        <div class="recent_works3 recentwork_carousel__crsl js-slick" data-slick='{
          "infinite": true,
          "slidesToShow": 7,
          "slidesToScroll": 1,
          "swipeToSlide": true,
          "autoplay": true,
          "autoplaySpeed": 1200,
          "speed": 500,
          "cssEase": "ease-in-out",
          "arrows": true,
          "appendArrows": ".recentwork_carousel_v3 .hgSlickNav",
          "responsive": [
            {
              "breakpoint": 1199,
              "settings": {
                "slidesToShow": 7
              }
            },
            {
              "breakpoint": 767,
              "settings": {
                "slidesToShow": 2
              }
            },
            {
              "breakpoint": 480,
              "settings": {
                "slidesToShow": 1
              }
            }
          ]
        }'>
          <!-- Item #1 -->
          <?php
            $args = [
                'posts_per_page'    => '-1',
                'post_type'         => ['partner'],
                'post_parent'       => 0,
                'order'             => 'DESC',
                'suppress_filters'  => false,
            ];
            $partners = new \WP_Query( $args ); 
            wp_reset_postdata();
          ?>
          <?php foreach ($partners -> posts as $key => $p): ?>
            <?php $pimg = wp_get_attachment_image_src( get_post_thumbnail_id( $p -> ID ), 'large' ,true ); ?>
            <div class="recent-work_carousel-item">
              <!-- Portfolio link container -->
              <a href="<?=$p -> link; ?>" target="_blank" class="recentwork_carousel__link">
                <!-- Hover container -->
                <div class="hover recentwork_carousel__hover">
                  <!-- Image with custom height -->
                  <div style="height: 230px; ">
                    <img src="<?=$pimg[0];?>" style="object-fit: contain;" class="recentwork_carousel__img cover-fit-img" alt="" title="" />
                  </div>
                  <!--/ Image with custom height -->

                  <!-- Hover shadow overlay -->
                  <span class="hov recentwork_carousel__hov"></span>
                  <!--/ Hover shadow overlay -->
                </div>
                <!--/ Hover container -->

                <!-- Content details -->
                <div class="details recentwork_carousel__details">
                  <!-- Title -->
                  <h4 class="recentwork_carousel__crsl-title"><?=$p -> post_title;?></h4>
                  <!--/ Title -->
                </div>
                <!--/ Content details -->
              </a>
              <!--/ Portfolio link container -->
            </div>
          <?php endforeach ?>

          <!--/ Item #1 -->


        </div>
        <!--/ .recent_works3 .recentwork_carousel__crsl .js-slick -->
      </div>
      <!--/ Carousel wrapper - .recentwork_carousel__crsl-wrapper -->
    </div>
    <!--/ Recent Work carousel 3 default style element -->
  </div>
  <!--/ container-fluid -->
</section>
<!--/ Recent Work carousel element - full width section -->
	<!--/ Partners & Testimonials section with custom paddings -->


<?php get_footer(); ?>
    