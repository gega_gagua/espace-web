<?php 

/* 
* Template Name: About
*/

get_header();
global $post;
$history = [
	[
		'add2_year' => 2016,
		'add2_title' => 'პირველი საჯარო მოხმარების დამტენები საქართველოში'
	],
	[
		'add2_year' => 2017,
		'add2_title' => 'პირველი სწრაფი დამტენის მონტაჟი საქართველოში'
	],
	[
		'add2_year' => 2018,
		'add2_title' => 'KEBA ოფიციალური წარმომადგენელი საქართველოში'
	],
	[
		'add2_year' => 2019,
		'add2_title' => 'ABB სწრაფი დამტენების მომსახურების ლიცენზია'
	],
	[
		'add2_year' => 2020,
		'add2_title' => 'ახალი აპლიკაცია მომხმარებლებისთვის'
	],
	[
		'add2_year' => 2021,
		'add2_title' => 'პირველად საქართველოში აპლიკაცია ბიზნესისთვის, თანხის გამომუშავების შესაძლებლობით'
	]
];
?>


<!-- Page Sub-Header + mask style 6 -->
<div id="page_header" class="page-subheader site-subheader-cst uh_hg_def_header_style maskcontainer--mask6">
	<div class="bgback"></div>

	<!-- Background -->
	<div class="kl-bg-source">
		<!-- Video background container -->
		<div class="kl-video-container kl-bg-source__video">
			<!-- Video wrapper -->
			<div class="kl-video-wrapper video-grid-overlay">
				<!-- Self Hosted Video Source -->
				<div class="kl-video valign halign" style="width: 100%; height: 100%;" data-setup='{ 
	                  "position": "absolute", 
	                  "loop": true, 
	                  "autoplay": true, 
	                  "muted": true, 
	                  "youtube": "ql94IYUrwXY", 
	                  "poster": "videos/Working-Space.jpg", 
	                  "video_ratio": "1.7778" }'>
				</div>
				<!--/ Self Hosted Video Source -->
			</div>
			<!--/ Video wrapper -->
		</div>
		<!--/ Video background container -->

		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background: rgba(130,36,227,0.3); background: -moz-linear-gradient(left, rgba(130,36,227,0.3) 0%, rgba(51,158,221,0.4) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(130,36,227,0.3)), color-stop(100%,rgba(51,158,221,0.4))); background: -webkit-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -o-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -ms-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: linear-gradient(to right, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); ">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background -->

	<!-- Animated Sparkles -->
	<div class="th-sparkles"></div>
	<!--/ Animated Sparkles -->

	<!-- Sub-Header content wrapper -->
	<div class="ph-content-wrap d-flex">
		<div class="container align-self-center">
			<div class="row">
				<div class="col-sm-12 col-md-6 col-lg-6">
					<!-- Breadcrumbs -->
					<ul class="breadcrumbs fixclear">
						<li><a href="index.html">Home</a></li>
						<li>PAGES – ABOUT US</li>
					</ul>
					<!--/ Breadcrumbs -->

					<!-- Current date -->
					<span  class="subheader-currentdate"><?=date('d m, Y'); ?></span>
					<!--/ Current date -->

					<div class="clearfix"></div>
				</div>
				<!--/ col-sm-12 col-md-6 col-lg-6 -->

				<div class="col-sm-12 col-md-6 col-lg-6">
					<!-- Sub-header titles -->
					<div class="subheader-titles">
						<!-- Main Title -->
						<h2 class="subheader-maintitle">
							ABOUT OUR COMPANY
						</h2>
						<!--/ Main Title -->

						<!-- Main Sub-Title -->
						<h4 class="subheader-subtitle">
							GET TO KNOW US BETTER
						</h4>
						<!--/ Main Sub-Title -->
					</div>
					<!--/ Sub-header titles -->
				</div>
				<!--/ col-sm-12 col-md-6 col-lg-6 -->
			</div>
			<!--/ row -->
		</div>
		<!--/ .container .align-self-center -->
	</div>
	<!--/ Sub-Header content wrapper .d-flex -->

	<!-- Sub-Header bottom mask style 6 -->
	<div class="kl-bottommask kl-bottommask--mask6">
		<svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
			<defs>
				<filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask6">
					<feOffset dx="0" dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
					<feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
					<feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" in="shadowBlurOuter1" type="matrix" result="shadowMatrixOuter1"></feColorMatrix>
					<feMerge>
						<feMergeNode in="shadowMatrixOuter1"></feMergeNode>
						<feMergeNode in="SourceGraphic"></feMergeNode>
					</feMerge>
				</filter>
			</defs>
			<g transform="translate(-1.000000, 10.000000)">
				<path d="M0.455078125,18.5 L1,47 L392,47 L1577,35 L392,17 L0.455078125,18.5 Z" fill="#000000"></path>
				<path d="M2701,0.313493752 L2701,47.2349598 L2312,47 L391,47 L2312,0 L2701,0.313493752 Z" fill="#fbfbfb" class="bmask-bgfill" filter="url(#filter-mask6)"></path>
				<path d="M2702,3 L2702,19 L2312,19 L1127,33 L2312,3 L2702,3 Z" fill="#cd2122" class="bmask-customfill"></path>
			</g>
		</svg>
	</div>
	<!--/ Sub-Header bottom mask style 6 -->
</div>
<!--/ Page Sub-Header + mask style 6 -->

<!-- Title + Services + Skills Diagram section with custom paddings -->
<section class="hg_section pt-80 pb-50">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
					<!-- Title with custom font, size and weight -->
					<h3 class="tbk__title kl-font-alt fs-xl fw-bold text-center">
						<?=$post -> post_title;?>
					</h3>
					<!--/ Title -->
				</div>

				<!-- separator -->
				<div class="hg_separator clearfix mb-60">
				</div>
				<!--/ separator -->
			</div>
			<!--/ col-sm-12 col-md-12 -->

			<div class="col-sm-12 col-md-12 col-lg-">
				<div class="row">
					<div class="col-sm-12 col-md-12 col-lg-12">
						<!-- Icon box float left -->
						<?=apply_filters('the_content', $post -> post_content);?>
						<!--/ Icon box float left -->
					</div>
					<!--/ col-sm-12 col-md-6 col-lg-6 -->
				</div>
				<!--/ row -->
			</div>
			<!--/ col-sm-12 col-md-12 col-lg-6 -->


		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Title + Services + Skills Diagram section with custom paddings -->


<section class="hg_section bg-white">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
					<!-- Title with custom font, size and weight -->
					<h3 class="tbk__title kl-font-alt fs-xl fw-bold text-center">
						ჩვენი სერვისები
					</h3>
					<!--/ Title -->
				</div>

				<!-- separator -->
				<div class="hg_separator clearfix mb-60">
				</div>
				<!--/ separator -->
			</div>
			<!--/ col-sm-12 col-md-12 -->
			<div class="col-lg-10 offset-lg-1">
				<div class="row gutter-md">
					<div class="col-sm-12 col-md-6 col-lg-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper">
									<img src="<?=get_template_directory_uri();?>/assets/images/set-03-01.svg" class="kl-iconbox__icon" alt="Stunning Template">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-m fw-normal gray2">
											დამტენების მონტაჟი
										</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Set up pages and content like a <strong>PRO</strong>. Coding is not required and a handy documentation is included.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
						<!--/ Icon box float left -->
					</div>
					<!--/ col-sm-12 col-md-6 col-lg-3 -->

					<div class="col-sm-12 col-md-6 col-lg-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper">
									<img class="kl-iconbox__icon" src="<?=get_template_directory_uri();?>/assets/images/set-03-02.svg" alt="Iconic Awarded Design">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-m fw-normal gray2">
											მონტაჟის შემდგომი მომსახურება
										</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Our design is featured across multiple marketplaces and awarded for its looks. Walk-through and enjoy the visuals.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
						<!-- Icon box float left -->
					</div>
					<!--/ col-sm-12 col-md-6 col-lg-3 -->

					<div class="col-sm-12 col-md-6 col-lg-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper">
									<img class="kl-iconbox__icon" src="<?=get_template_directory_uri();?>/assets/images/set-03-03.svg" alt="Featurewise Complete">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-m fw-normal gray2">
											ინსპექტირება
										</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											Without a doubt, Kallyas is one of the most complete template on the market, being packed with all the goodies and sweet gems.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
						<!--/ Icon box float left -->
					</div>
					<!--/ col-sm-12 col-md-6 col-lg-3 -->

					<div class="col-sm-12 col-md-6 col-lg-3">
						<!-- Icon box float left -->
						<div class="kl-iconbox kl-iconbox--fleft text-left">
							<div class="kl-iconbox__inner">
								<!-- Icon -->
								<div class="kl-iconbox__icon-wrapper ">
									<img class="kl-iconbox__icon" src="<?=get_template_directory_uri();?>/assets/images/set-03-04.svg" alt="Mature project">
								</div>
								<!--/ Icon -->

								<!-- /.kl-iconbox__icon-wrapper -->
								<div class="kl-iconbox__content-wrapper">
									<!-- Title -->
									<div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
										<h3 class="kl-iconbox__title fs-m fw-normal gray2">
											კონსულტაცია
										</h3>
									</div>
									<!--/ Title -->

									<!-- Description -->
									<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
										<p class="kl-iconbox__desc fs-14 gray">
											In time, gathering awesome feedback from our loyal customers, Kallyas became a mature, stable and future-proof project.
										</p>
									</div>
									<!--/ Description -->
								</div>
								<!-- /.kl-iconbox__content-wrapper -->
							</div>
							<!--/ kl-iconbox__inner -->
						</div>
					</div>
					<!--/ col-sm-12 col-md-6 col-lg-3 -->
				</div>
				<!--/ row gutter-md -->
			</div>
			<!--/ col-lg-10 offset-lg-1 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container-fluid -->
</section>

<!-- Separator -->
<section class="hg_section p-0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<!-- separator margin bottom 60px -->
				<div class="hg_separator clearfix mb-60">
				</div>
				<!--/ separator -->
			</div>
			<!--/ col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Separator -->


<!-- Historic section with custom paddings -->
<section class="hg_section pt-30 pb-30">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- Historic timeline element -->
				<div class="timeline_bar">
					<div class="row">
						
						<?php foreach ($history as $key => $value): ?>
							<!-- Timeline box 1 -->
							<div class=" col-sm-12 col-md-6 <?=($key%2==0) ? 'offset-md-6' : '';?>" data-align="<?=($key%2==0) ? 'right' : 'left';?>">
								<!-- Timeline box -->
								<div class="timeline_box brand-bg">
									<!-- Date -->
									<div class="date">
										<?=$value['add2_year'];?>
									</div>
									<!--/ Date -->

									<!-- Title -->
									<h4 class="htitle">
										<?php if ($value['add2_icon']): ?>
											<img style="height: 80px;width: 80px; object-fit: cover;" src="<?=$value['add2_icon'];?>" alt="">
										<?php endif ?>
										<?=$value['add2_title'];?>
									</h4>
									<!--/ Title -->
								</div>
								<!--/ Timeline box -->
							</div>
							<!--/ Timeline box 1 -->							
						<?php endforeach ?>					

						<!-- End timeline -->
						<div class="col-sm-12 end_timeline">
							<span><?=__('PRESENT', 'gg'); ?></span>
						</div>
						<!--/ End timeline -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Historic timeline element -->
			</div>
			<!--/ col-sm-12 col-md-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Historic section with custom paddings -->


<section class="hg_section hg_section--relative pt-100 pb-120">
	<!-- Background with parallax effect -->
	<div class="kl-bg-source kl-bgSource-imageParallax js-KyHtmlParallax is-fixed is-visible" style="height: 578px; width: 1920px; transform: translate3d(0px, 135.031px, 0px);">
		<!-- Background image -->
		<div class="kl-bg-source__bgimage" style="background-image: url(http://vue.ge/gega/products/espace/wp-content/uploads/2020/11/6.jpg); background-repeat: no-repeat; background-attachment: scroll; background-position: 50% 50%; background-size: cover; transform: translate3d(0px, -104px, 0px);">
		</div>
		<!--/ Background image -->

		<!-- Gradient overlay -->
		<div class="kl-bg-source__overlay" style="background: rgba(0,0,0,0.85); background: -moz-linear-gradient(left, rgba(0,0,0,0.85) 0%, rgba(39,0,48,0.5) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(0,0,0,0.85)), color-stop(100%,rgba(39,0,48,0.5))); background: -webkit-linear-gradient(left, rgba(0,0,0,0.85) 0%,rgba(39,0,48,0.5) 100%); background: -o-linear-gradient(left, rgba(0,0,0,0.85) 0%,rgba(39,0,48,0.5) 100%); background: -ms-linear-gradient(left, rgba(0,0,0,0.85) 0%,rgba(39,0,48,0.5) 100%); background: linear-gradient(to right, rgba(0,0,0,0.85) 0%,rgba(39,0,48,0.5) 100%);">
		</div>
		<!--/ Gradient overlay -->
	</div>
	<!--/ Background with parallax effect -->

	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- Title element -->
				<div class="kl-title-block tbk--text-light text-center tbk-symbol--line">
					<!-- Title -->
					<h3 class="tbk__title">
						წლის ფაქტები
					</h3>
					<!--/ Title -->

					<!-- Title bottom symbol -->
					<div class="tbk__symbol ">
						<span></span>
					</div>
					<!--/ Title bottom symbol -->
				</div>
				<!--/ Title element -->

				<!-- Statistics element - normal placement & light box style -->
				<div class="statistic-box__container statistic-box--stl-style2 statistic-box--light">
					<!-- Statistic box #1 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon .icon-noun_65754 -->
							<span class="statistic-box__icon icon-noun_65754"></span>
						</div>
						<!--/ Icon wrapper -->

						<div class="statistic-box__line">
						</div>

						<!-- Statistic box details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">
								90+
							</h4>
							<!--/ Title -->

							<!-- Content/Description -->
							<div class="statistic-box__content">
								პარტნიორი კომპანიები
							</div>
							<!--/ Content/Description -->
						</div>
						<!--/ Statistic box details -->
					</div>
					<!--/ Statistic box #1 -->

					<!-- Statistic box #2 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon .icon-noun_61152 -->
							<span class="statistic-box__icon icon-noun_61152"></span>
						</div>
						<!--/ Icon wrapper -->

						<div class="statistic-box__line">
						</div>

						<!-- Statistic box details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">
								150+
							</h4>
							<!--/ Title -->

							<!-- Content/Description -->
							<div class="statistic-box__content">
								 დამონტაჟებული დამტენები  
							</div>
							<!--/ Content/Description -->
						</div>
						<!--/ Statistic box details -->
					</div>
					<!--/ Statistic box #2 -->

					<!-- Statistic box #3 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<span class="statistic-box__icon icon-gi-ico-10"></span>
						</div>
						<!-- Icon wrapper -->

						<div class="statistic-box__line">
						</div>

						<!-- Statistic box details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">
								80+
							</h4>
							<!--/ Title -->

							<!-- Content/Description -->
							<div class="statistic-box__content">
								 საჯარო დამტენები
							</div>
							<!--/ Content/Description -->
						</div>
						<!--/ Statistic box details -->
					</div>
					<!--/ Statistic box #3 -->

					<!-- Statistic box #4 -->
					<div class="statistic-box">
						<!-- Icon wrapper -->
						<div class="statistic-box__icon-holder">
							<!-- Icon .icon-noun_167805 -->
							<span class="statistic-box__icon icon-noun_167805"></span>
						</div>
						<!--/ Icon wrapper -->

						<div class="statistic-box__line">
						</div>

						<!-- Statistic box details -->
						<div class="statistic-box__details">
							<!-- Title -->
							<h4 class="statistic-box__title">
								455000+
							</h4>
							<!--/ Title -->

							<!-- Content/Description -->
							<div class="statistic-box__content">
								ნახშირორჟანგი, რომელიც არ გავრცელდა
							</div>
							<!--/ Content/Description -->
						</div>
						<!--/ Statistic box details -->
					</div>
					<!--/ Statistic box #4 -->
				</div>
				<!--/ Statistics element - normal placement & light box style -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>


	<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/plugins/parallax/KyHtmlParallax.js"></script>
	<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/plugins/parallax/parallax.js"></script>
<?php get_footer(); ?>
    