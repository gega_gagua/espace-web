<?php 

/* 
* Template Name: Simple List
*/

get_header();
global $post;
$apps = $post -> pages;
?>

<!-- Image Boxes element - Style 4 - section with custom paddings -->
<section class="hg_section bg-white pt-120 pb-80">
  <div class="container pt-80">
    <div class="row">
      <?php if (count($apps) > 0): ?>
        <?php foreach ($apps as $key => $appId): ?>
          <?php 
            $app = get_post($appId); 
            $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $appId ), 'large' ,true );
          ?>
          <div class="col-sm-12 col-md-6 col-lg-4">
            <!-- Image box element style 4 - left title -->
            <div class="box image-boxes imgboxes_style4 kl-title_style_left">
              <!-- Image box link wrapper -->
              <a href="<?=get_permalink($appId);?>" target="_self" class="imgboxes4_link imgboxes-wrapper" title="">
                <!-- Image -->
                <img src="<?=$bimage[0];?>" class="img-fluid imgbox_image cover-fit-img" alt="" title="" />
                <!--/ Image -->

                <!-- Border helper -->
                <span class="imgboxes-border-helper"></span>
                <!--/ Border helper -->

                <!-- Title -->
                <h3 class="m_title imgboxes-title">
                  <?=$app -> post_title;?>
                </h3>
                <!--/ Title -->
              </a>
              <!--/ Image box link wrapper -->

              <!-- Content -->
              <p>
                <?=$app -> text;?>
              </p>
              <!--/ Content -->
            </div>
            <!--/ Image box element style 4 - center title -->
          </div>  
        <?php endforeach ?>
      <?php endif ?>
    </div>
    <!--/ row -->
  </div>
  <!--/ container -->
</section>
<!--/ Image Boxes element - Style 4 - section with custom paddings -->


<?php get_footer(); ?>