<?php 

/* 
* Template Name: List
*/
get_header();
global $post;

$args = array (
  'post_type'              => array( 'product' ),
  'post_status'            => array( 'publish' ),
  'post_parent'			   => 0,
  'posts_per_page'         => '-1',
  'order'                  => 'ASC',
  //'offset'                 => $paged,
);

$products = new WP_Query( $args );
?>

<!-- Portfolio sortable section with custom paddings -->
<section class="hg_section pt-120 pb-80">
	<div class="container pt-80">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Portfolio sortable element -->
				<div class="hg-portfolio-sortable">
					<!-- Sorting toolbar -->
					<div id="sorting" class="fixclear">
						<span class="sortTitle"> Sort By: </span>
						<ul id="sortBy" class="option-set " data-option-key="sortBy" data-default="">
							<li><a href="#sortBy=name" data-option-value="name">Name</a></li>
							<li><a href="#sortBy=date" data-option-value="date" class="selected">Date</a></li>
						</ul>
					</div>
					<!--/ Sorting toolbar -->

					<!-- Portfolio navigation list -->
					<ul id="portfolio-nav" class="fixclear">
						<!-- Nav all -->
						<li class="current"><a href="#" data-filter="*">ყველა</a></li>

						<!-- Nav #1 / Apps = .apps_sort -->
						<li class=""><a href="#" data-filter=".charger_sort">დამტენები</a></li>

						<!-- Nav #2 / Branding = .branding_sort -->
						<li class=""><a href="#" data-filter=".charger_sort">დამტენი სადენი</a></li>

						<!-- Nav #3 / eCommerce = .ecommerce_sort -->
						<li class=""><a href="#" data-filter=".unu_sort">UNU ელექტრო სკუტერი</a></li>

						<!-- Nav #4 / Illustration = .illustration_sort -->
						<li class=""><a href="#" data-filter=".case_sort">NUTCASE ჩაფხუტები</a></li>
					</ul>
					<!--/ Portfolio navigation list -->

					<div class="clear">
					</div>

					<!-- Portfolio thumbs (1 to 4 columns) -->
					<ul id="thumbs" class="fixclear pt-50" data-columns="4">
						
						<?php foreach ($products -> posts as $key => $p): ?>
							<!-- Item #1 - filter category: miscellaneous_sort, web_sort and date: 01/01/2017 (mm/dd/yyyy) -->
							<?php 
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $p -> ID ), 'large' ,true ); 
								$content = strip_tags( $p -> post_content );
								$content = mb_substr($content, 0, 100);
								$terms = get_the_terms( $p -> ID, 'product_cat' );
							?>
							<li class="item kl-has-overlay <?=$terms[0] -> name;?>_sort" data-date="01/01/2017">
								<!-- Item wrapper -->
								<div class="inner-item">
									<!-- Intro image -->
									<div class="img-intro">
										<?php if ($p -> _regular_price): ?>
											<div class="hg_badge_container">
												<span class="hg_badge_sale">
													<?=$p -> _regular_price; ?> ლ
												</span>
												<!-- <span class="hg_badge_new">NEW!</span> -->
											</div>											
										<?php endif ?>

										<!-- Image -->
										<img style="height: 180px; object-fit: cover;" src="<?=$image[0];?>" alt="<?=$p -> post_title; ?>" title="<?=$p -> post_title; ?>" />

										<!-- Overlay -->
										<div class="overlay">
											<a href="<?=get_permalink($p -> ID);?>">
											<div class="overlay-inner">
												<!-- Icon = .far fa-image -->
												<span class="far fa-image"></span>
											</div>
											</a>
										</div>
										<!--/ Overlay -->
									</div>
									<!--/ Intro image -->

									<!-- Title -->
									<h4 class="title">
										<a href="<?=get_permalink($p -> ID);?>">
											<span class="name" style="height: 44px; display: block;">
												<?=$p -> post_title; ?>
											</span>
										</a>
									</h4>

									<!-- Description -->
									<span class="moduleDesc" style="height: 107px; overflow: hidden;">
										<?=$content;?>...
									</span>

									<div class="clear"></div>
								</div>
								<!--/ Item wrapper (.inner-item) -->
							</li>
						<!--/ Item #1 -->
						<?php endforeach ?>

					</ul>
					<!--/ Portfolio thumbs (1 to 4 columns) -->
				</div>
				<!--/ Portfolio sortable element -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Portfolio sortable section with custom paddings -->


<!-- JS FILES // Loaded on this page -->
<!-- Required script for sorting (masonry) elements - Isotope filter -->
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/plugins/jquery.isotope.min.js"></script>

<!-- Required js trigger for Portfolio sortable element -->
<script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/trigger/kl-portfolio-sortable.js"></script>


<script type="text/javascript">
	$(window).on('load', function() {
		if(window.location.search) {
			setTimeout(function() {
		      	var hash = Number( window.location.search.replace('?active=', '') );
		    	if (hash) {
		    		$('#portfolio-nav a:eq('+hash+')').trigger('click')
		    	}
	    	}, 1500);
	  	}
  	});
</script>
<?php get_footer() ?>