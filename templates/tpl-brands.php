<?php 

/* 
* Template Name: Brands
*/

get_header();
global $post;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
?>

<!-- Page Sub-Header -->
<?php include get_template_directory() . '/templates/partials/headline.php'; ?>
<!--/ Page sub-header -->


<?php 
$args = array (
  'post_type'              => array( 'brand' ),
  'post_status'            => array( 'publish' ),
  'post_parent'			   => 0,
  'posts_per_page'         => '-1',
  'order'                  => 'ASC',
  //'offset'                 => $paged,
);

$brands = new WP_Query( $args );

?>

<!-- Portfolio sortable section with custom paddings -->
<section class="hg_section pt-80 pb-80">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<!-- Portfolio sortable element -->
				<div class="hg-portfolio-sortable">
					<!-- Sorting toolbar -->
<!-- 					<div id="sorting" class="fixclear">
						<span class="sortTitle"> Sort By: </span>
						<ul id="sortBy" class="option-set " data-option-key="sortBy" data-default="">
							<li><a href="#sortBy=name" data-option-value="name">Name</a></li>
							<li><a href="#sortBy=date" data-option-value="date" class="selected">Date</a></li>
						</ul>
						<span class="sortTitle"> Direction: </span>
						<ul id="sort-direction" class="option-set" data-option-key="sortAscending">
							<li><a href="#sortAscending=true" data-option-value="true" class="selected">ASC</a></li>
							<li><a href="#sortAscending=false" data-option-value="false">DESC</a></li>
						</ul>
					</div> -->
					<!--/ Sorting toolbar -->

					<!-- Portfolio navigation list -->
					<ul id="portfolio-nav" class="fixclear">
						<!-- Nav all -->
						<li class="current"><a href="#" data-filter="*">ყველა</a></li>

						<!-- Nav #1 / Apps = .apps_sort -->
						<li class=""><a href="#" data-filter=".apps_sort">კოსმეტიკა</a></li>

						<!-- Nav #2 / Branding = .branding_sort -->
						<li class=""><a href="#" data-filter=".branding_sort">პარფიუმერია</a></li>

						<!-- Nav #3 / eCommerce = .ecommerce_sort -->
						<li class=""><a href="#" data-filter=".ecommerce_sort">თმის მოვლა</a></li>

						<!-- Nav #4 / Illustration = .illustration_sort -->
						<li class=""><a href="#" data-filter=".illustration_sort">კანის მოვლა</a></li>

						<!-- Nav #5 / Miscellaneous = .miscellaneous_sort -->
						<li class=""><a href="#" data-filter=".miscellaneous_sort">ფრჩხილის მოვლა</a></li>

						<!-- Nav #6 / WEB = .web_sort -->
						<li class=""><a href="#" data-filter=".web_sort">ბავშვის მოვლა</a></li>
					</ul>
					<!--/ Portfolio navigation list -->

					<div class="clear">
					</div>

					<!-- Portfolio thumbs (1 to 4 columns) -->
					<ul id="thumbs" class="fixclear" data-columns="4">
	
						<?php foreach ($brands -> posts as $key => $brand): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $brand -> ID ), 'medium' ,true ); ?>
							<!-- Item #1 - filter category: miscellaneous_sort, web_sort and date: 01/01/2017 (mm/dd/yyyy) -->
							<li class="item kl-has-overlay miscellaneous_sort web_sort" data-date="01/01/2017">
								<!-- Item wrapper -->
								<div class="inner-item">
									<!-- Intro image -->
									<div class="img-intro">
										<!-- Pop-up image -->
										<!-- <a href="images/frame.html" data-type="image" data-lightbox="image" class="hoverLink"></a> -->

										<!-- Image -->
										<img style="    height: 120px;
													    object-fit: contain;
													    width: 90%;
													    padding-top: 25px;
													    margin: 20px auto 5px auto;
													    display: block;" src="<?=$image[0];?>" alt="portfolio" title="portfolio" />

										<!-- Overlay -->
										<div class="overlay" style="display: none;">
											<div class="overlay-inner">
												<!-- Icon = .far fa-image -->
												<span class="far fa-image"></span>
											</div>
										</div>
										<!--/ Overlay -->
									</div>
									<!--/ Intro image -->

									<!-- Title -->
									<h4 class="title">
										<a href="portfolio-item.html">
											<span class="name">
												<?=$brand -> post_title;?>
											</span>
										</a>
									</h4>

									<!-- Description -->
									<span class="moduleDesc">
										A minimal design is usually best for portfolios, projects and case studies. A clean, uncluttered design allows featured works to stand out...
									</span>

									<div class="clear"></div>
								</div>
								<!--/ Item wrapper (.inner-item) -->
							</li>
							<!--/ Item #1 -->
						<?php endforeach ?>

					</ul>
					<!--/ Portfolio thumbs (1 to 4 columns) -->
				</div>
				<!--/ Portfolio sortable element -->
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Portfolio sortable section with custom paddings -->



<!-- JS FILES // Loaded on this page -->
<!-- Required script for sorting (masonry) elements - Isotope filter -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/plugins/jquery.isotope.min.js"></script>

<!-- Required js trigger for Portfolio sortable element -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/trigger/kl-portfolio-sortable.js"></script>

<!-- Custom Kallyas JS codes -->
<!-- <script type="text/javascript" src="js/kl-scripts.js"></script> -->


<?php get_footer(); ?>