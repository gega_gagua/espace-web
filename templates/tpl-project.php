<?php 

/* 
* Template Name: Partners
*/

get_header();
global $post;
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
$args = [
    'posts_per_page'    => '-1',
    'post_type'         => ['partner'],
    'post_parent'       => 0,
    'order'             => 'DESC',
    'suppress_filters'  => false,
];
$promotions = new \WP_Query( $args ); 
wp_reset_postdata();
?>



<!-- Portfolio sortable section with custom paddings -->
<section class="hg_section pt-120 pb-80">
    <div class="container pt-50">
    <div class="row">
      <div class="col-md-12 col-sm-12">

        <h3 class="global-title">
            <?=$post -> post_title; ?>
        </h3>
        <!-- Portfolio sortable element -->
        <div class="hg-portfolio-sortable">

          <!-- Portfolio thumbs (1 to 4 columns) -->
          <ul id="thumbs" class="fixclear" data-columns="4">
  
            <?php foreach ($promotions -> posts as $key => $p): ?>
              <?php $pimg = wp_get_attachment_image_src( get_post_thumbnail_id( $p -> ID ), 'large' ,true ); ?>
              <!-- Item #1 - filter category: miscellaneous_sort, web_sort and date: 01/01/2017 (mm/dd/yyyy) -->
              <li class="item kl-has-overlay miscellaneous_sort web_sort" >
                <!-- Item wrapper -->
                <a href="<?=$p -> link; ?>" target="_blank">
                  <div class="inner-item" style="padding-top: 30px;">
                    <!-- Intro image -->
                    <div class="img-intro">
                      <!-- Pop-up image -->

                      <!-- Image -->
                      <img style="height: 120px;
                                max-width: 90%;
                                object-fit: contain;
                                margin: 20px auto 5px auto;
                                display: block;" src="<?=$pimg[0];?>" alt="<?=$p -> post_title;?>" title="<?=$p -> post_title;?>" />

                      <!-- Overlay -->
                      <div class="overlay" style="display: none;">
                        <div class="overlay-inner">
                          <!-- Icon = .far fa-image -->
                          <span class="far fa-image"></span>
                        </div>
                      </div>
                      <!--/ Overlay -->
                    </div>
                    <!--/ Intro image -->

                    <!-- Title -->
                    <h4 class="title text-center" style="height: 88px; overflow: hidden;">
    
                      <span class="name">
                        <?=$p -> post_title;?>
                      </span>

                    </h4>
                    <div class="clear"></div>
                  </div>
                </a>
                <!--/ Item wrapper (.inner-item) -->
              </li>
              <!--/ Item #1 -->
            <?php endforeach ?>

          </ul>
          <!--/ Portfolio thumbs (1 to 4 columns) -->
        </div>
        <!--/ Portfolio sortable element -->
      </div>
      <!--/ col-md-12 col-sm-12 -->
    </div>
    <!--/ row -->
  </div>
  <!--/ container -->
</section>
<!--/ Portfolio sortable section with custom paddings -->



<!-- JS FILES // Loaded on this page -->
<!-- Required script for sorting (masonry) elements - Isotope filter -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/plugins/jquery.isotope.min.js"></script>

<!-- Required js trigger for Portfolio sortable element -->
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/trigger/kl-portfolio-sortable.js"></script>

<!-- Custom Kallyas JS codes -->
<!-- <script type="text/javascript" src="js/kl-scripts.js"></script> -->


<?php get_footer(); ?>