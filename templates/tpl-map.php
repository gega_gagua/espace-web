<?php 

/* 
* Template Name: Map
*/

get_header();
global $post;
$map = $post -> map;
$map = json_encode($map);
?>



<!-- Google maps slideshow element + Bottom mask style 5 -->
<section class="map-section">
	<div class="container">
		<div class="maps-left-container col-sm-12 col-md-12">
			<a href="1" target="_self" class="btn-element btn btn-fullwhite d-inline-flex mr-20 mb-0 active" title="ყველა დამტენი">
				<span>ყველა დამტენი</span>
			</a>
			<a href="2" target="_self" class="btn-element btn btn-fullwhite d-inline-flex mr-20 mb-0" title="სტანდარტული">
				<span>სტანდარტული დამტენი</span>
			</a>
			<a href="3" target="_self" class="btn-element btn btn-fullwhite d-inline-flex mr-20 mb-0" title="სწრაფი">
				<span>სწრაფი დამდენი</span>
			</a>
		</div>
	</div>
</section>

<div class="kl-slideshow static-content__slideshow scontent__maps">

	<!-- Map with custom height -->
	<div class="th-google_map" data-map="" style="height: 750px;">
	</div>
	<!-- end map -->
</div>

<!-- JS FILES // Loaded on this page -->
<!-- Required js scripts files for Google Maps element (create and use your API Key bellow) https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8EM5gUzYRSDi2KQPGPZEiqshGX8TDb1U"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/assets/js/plugins/jquery.gmap.min.js"></script>
<script>
    var data = <?php echo $map ?>; // Don't forget the extra semicolon!
</script>
<!-- Requried js trigger file for Google Maps element -->
<script type="text/javascript">
	;(function($){
	"use strict";

	$(document).ready(function() {
		var mapMarkers = [];
		data.forEach((item) => {
			mapMarkers.push({
				// address: "Batumi",
				latitude: Number(item.mlocation),
				longitude: Number(item.mlocatioy),
				mchec: item.mchec,
				icon: {
					image: (item.mchec) ? "<?php echo get_template_directory_uri();?>/assets/images/map-marker.png" : "<?php echo get_template_directory_uri();?>/assets/images/charger.png",
					iconsize: [39, 45], // w, h
					iconanchor: [0, 0] // x, y
				},
				html: '<div style="width: 200px; padding: 5px; font-size:15px;">' + item.mtitle +'</div>',
			})
		})

		
		// Map Initial Location
		var initLatitude = 42.0627887;
		var initLongitude = 43.887784;

		var options = {
			controls: {
				panControl: true,
				zoomControl: true,
				zoomControlOptions: {
					position: google.maps.ControlPosition.RIGHT_CENTER
				},
				mapTypeControl: false,
				scaleControl: true,
				streetViewControl: true,
				streetViewControlOptions: {
					position: google.maps.ControlPosition.RIGHT_CENTER
				},
				overviewMapControl: true
			},
			scrollwheel: false,
			// maptype: 'HYBRID',
			markers: mapMarkers,
			latitude: initLatitude,
			longitude: initLongitude,
			zoom: 9,
			styles:[{"elementType":"geometry","stylers":[{"color":"#1d2c4d"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#8ec3b9"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#1a3646"}]},{"featureType":"administrative.country","elementType":"geometry.stroke","stylers":[{"color":"#4b6878"}]},{"featureType":"administrative.land_parcel","elementType":"labels.text.fill","stylers":[{"color":"#64779e"}]},{"featureType":"administrative.province","elementType":"geometry.stroke","stylers":[{"color":"#4b6878"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#334e87"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#023e58"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#283d6a"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#6f9ba5"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#023e58"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#3C7680"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#304a7d"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#2c6675"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#255763"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#b0d5ce"}]},{"featureType":"road.highway","elementType":"labels.text.stroke","stylers":[{"color":"#023e58"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#98a5be"}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"color":"#1d2c4d"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"color":"#283d6a"}]},{"featureType":"transit.station","elementType":"geometry","stylers":[{"color":"#3a4762"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#0e1626"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#4e6d70"}]}],
			draggable: Modernizr.touch ? false : true
		}
		// Map Extended Settings
		var map = jQuery(".th-google_map").gMap(options);

		$('.maps-left-container a').on('click', function(e){
			e.preventDefault();
			$('.map-section a').removeClass('active');
			$(this).addClass('active');
			var id = $(this).attr('href');
			var filteredMarkers = [];
			jQuery(".th-google_map").gMap('clearMarkers');
			if (id == 1) {
				filteredMarkers = mapMarkers;
			} else if (id == 2) {
				filteredMarkers = mapMarkers.filter(item => item.mchec !== "on")
			} else {
				filteredMarkers = mapMarkers.filter(item => item.mchec === "on")
			}
			
			filteredMarkers.forEach((item) => {
				jQuery(".th-google_map").gMap('addMarker', item);
			})
		})
	});

})(jQuery);
</script>


<?php get_footer(); ?>
    