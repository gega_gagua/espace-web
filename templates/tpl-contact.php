<?php 

/* 
* Template Name: Contact
*/

get_header();
global $post;
$options = get_option( 'general_options' );
?>

<!-- Contact form element & details section with custom paddings -->
<section class="hg_section pt-120 pb-80">
	<div class="container pt-50">
		<h3 class="global-title">
			<?=$post -> post_title; ?>
		</h3>
		<div class="row">
			<div class="col-sm-12 col-md-9 col-lg-9 mb-sm-30">
				<!-- Contact form element -->
				<div class="contactForm">
					<form action="php_helpers/_contact-process.php" method="post" class="contact_form row" enctype="multipart/form-data">
						<!-- Response wrapper -->
						<div class="cf_response"></div>

						<div class="col-sm-12 kl-fancy-form">
							<label class="control-label">
								FIRSTNAME
							</label>
							<input type="text" name="name" id="cf_name" class="form-control" placeholder="Please enter your first name" value="" tabindex="1" maxlength="35" required>
						</div>

						<div class="col-sm-12 kl-fancy-form">
							<label class="control-label">
								EMAIL
							</label>
							<input type="text" name="email" id="cf_email" class="form-control h5-email" placeholder="Please enter your email address" value="" tabindex="1" maxlength="35" required>
						</div>

						<div class="col-sm-12 kl-fancy-form">
							<label class="control-label">
								MESSAGE
							</label>
							<textarea name="message" id="cf_message" class="form-control" cols="30" rows="10" placeholder="Your message" tabindex="4" required></textarea>
						</div>

						<!-- Google recaptcha required site-key (change with yours => https://www.google.com/recaptcha/admin#list) -->
						<div class="g-recaptcha" data-sitekey="SITE-KEY"></div>
						<!--/ Google recaptcha required site-key -->

						<div class="col-sm-12 text-center">
							<!-- Contact form send button -->
							<button class="btn btn-fullcolor pl-40 pr-30" type="submit">
								Send <span class="pl-10">></span>
							</button>
						</div>
					</form>
				</div>
				<!--/ Contact form element -->
			</div>
			<!--/ col-sm-12 col-md-9 col-lg-9 mb-sm-30 -->

			<div class="col-sm-12 col-md-3 col-lg-3">
				<!-- Contact details -->
				<div class="text_box">
					<!-- Title -->
					<h3 class="text_box-title text_box-title--style2">
						CONTACT INFO
					</h3>

					<!-- Sub-title -->
					<p class="pt-30 pb-30">ზურაბ და თეიმურაზ ზალდასტანიშვილის N16, თბილისი</p>

					<p>
		                <strong>T  
		                  +995 32 243 34 73
		                </strong><br>
		                Email: <a href="mailto:info@e-space.ge">info@e-space.ge</a>
		            </p>

		            <ul class="social-icons sc--clean clearfix ml-0">
		                <li><a style="color: #535353;" href="https://www.facebook.com/espace.ge" target="_blank" class="fab fa-facebook-f" title="Facebook"></a></li>
		                <li><a style="color: #535353;" href="https://www.instagram.com/e_space.ge/?hl=en" target="_blank" class="fab fa-instagram" title="Facebook"></a></li>
		                <li><a style="color: #535353;" href="https://www.youtube.com/channel/UCfeHTLEnFdvroFTWw0tv9Bg?view_as=subscriber" target="_blank" class="fab fa-youtube" title="Facebook"></a></li>
		                <li><a style="color: #535353;" href="https://www.linkedin.com/company/ltd-e-space/" target="_blank" class="fab fa-linkedin-in" title="Facebook"></a></li>
		            </ul>
				</div>
				<!--/ Contact details -->
			</div>
			<!--/ col-sm-12 col-md-3 col-lg-3 -->
		</div>
		<!--/ .row -->
	</div>
	<!--/ .container -->
</section>
<!--/ Contact form element & details section with custom paddings -->


<?php get_footer(); ?>
    