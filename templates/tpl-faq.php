<?php 

/* 
* Template Name: FAQ
*/

get_header();
global $post;
?>

<!-- Page Sub-Header -->
<?php include get_template_directory() . '/templates/partials/headline.php'; ?>
<!--/ Page sub-header -->


<!-- FAQ (Accordions element style 3) section with custom paddings -->
<section class="hg_section pt-80">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- Accordions element style 3 -->
				<div class="hg_accordion_element style3">
					<!-- Title -->
					<h3 class="global-title">
						<?=$post -> post_title; ?>
					</h3>
					<!--/ Title -->

					<!-- Accordions wrapper -->
					<div class="th-accordion">

						<?php foreach ($post -> faq_benef as $key => $value): ?>
													<!-- Acc group #1 -->
							<div class="acc-group">
								<!-- Title group button acc #1 -->
								<div id="headingOne<?=$key;?>">
									<a data-toggle="collapse" data-target="#acc<?=$key;?>" class="<?=($key === 0) ? : 'collapsed'; ?>" aria-expanded="true" aria-controls="acc<?=$key;?>">
										<?=$value['car2_title']; ?><span class="acc-icon"></span>
									</a>
								</div>
								<!--/ Title group button acc #1 -->

								<!-- Acc #1 -->
								<div id="acc<?=$key;?>" class="collapse <?=($key > 0) ? : 'show'; ?> " aria-labelledby="headingOne<?=$key;?>" data-parent=".th-accordion">
									<!-- Content -->
									<div class="content">
										<!-- Description -->
										<p>
											<?=wpautop($value['car2_desc']); ?>
										</p>
										<!--/ Description -->
									</div>
									<!--/ Content -->
								</div>
								<!--/ Acc #1 -->
							</div>
							<!--/ Acc group #1 -->
						<?php endforeach ?>

					</div>
					<!--/ Accordions wrapper -->
				</div>
				<!--/ Accordions element style 3 -->
			</div>
			<!--/ col-sm-12 col-md-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ FAQ (Accordions element style 3) section with custom paddings -->


<!-- Separator element - section with custom paddings -->
<section class="hg_section pb-0">
	<div class="container">
		<div class="row">					
			<div class="col-md-12 col-sm-12">
				<!-- Separator -->
				<div class="hg_separator clearfix"></div>
			</div>
			<!--/ col-md-12 col-sm-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Separator element - section with custom paddings -->

<!-- Call to action element - section with custom paddings -->
<section class="hg_section pb-50">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-9 col-lg-9">
				<!-- Title element -->
				<div class="kl-title-block">
					<!-- Title with alternative font, custom size, theme color and bold style -->
					<h3 class="tbk__title kl-font-alt fs-l fw-bold tcolor">
						ONE OF THE MOST COMPLETE TEMPLATE
					</h3>

					<!-- Sub-title with custom size and thin style -->
					<h4 class="tbk__subtitle fs-s fw-thin">
						We always had this statement and we're keeping our promise. Beside a <span class="fw-semibold">powerful yet easy to use template</span>, Kallyas has packed inside lots of sweet features that wait to be discovered.
					</h4>
				</div>
				<!--/ Title element -->
			</div>
			<!--/ col-sm-12 col-md-9 col-lg-9 mb-sm-35 -->

			<div class="col-sm-12 col-md-3 co-lg-3 d-flex align-self-center justify-content-center">
				<!-- Button full color style -->
				<a href="#" target="_blank" class="btn-element btn btn-fullcolor btn-md btn-fullwidth" title="">
					<span>Contact Us</span>
				</a>
			</div>
			<!--/ col-sm-12 col-md-3 co-lg-3 d-flex align-self-center justify-content-center -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Call to action element - section with custom paddings -->




<?php get_footer(); ?>
    