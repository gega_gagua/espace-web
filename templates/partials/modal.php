<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document" style="margin-top: 10%;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">დაჯავშნა</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-container container m-t-50">
			<form action="<?=site_url();?>" id="contact_form">
				<div class="row">
					<div class="col-md-6">
						<div class="input-container">
							<label for="name"><?=__('Name','gg') ?></label>
							<input class="required" type="text" id="name" placeholder="<?php echo __('Your Name', 'gg'); ?>">
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-container">
							<label for="phone_number"><?=__('Phone','gg') ?></label>
							<input class="required" type="text" id="phone_number" placeholder="<?php echo __('Your phone', 'gg'); ?>">
						</div>
					</div>
				</div>	

				<div class="row">
					<div class="col-sm-12">
						<button type="submit" class="box box-green m-t-25 float-right">
							<?php echo __('Message Send', 'gg'); ?>
						</button>
					</div>
				</div>
			</form>
		</div>
      </div>
    </div>
  </div>
</div>