    <!-- Slideshow - iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->
    <div class="kl-slideshow iosslider-slideshow uh_light_gray maskcontainer--shadow_ud iosslider--custom-height scrollme">
      <!-- Loader -->
      <div class="kl-loader">
        <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px" viewbox="0 0 40 40" enable-background="new 0 0 40 40" xml:space="preserve"><path opacity="0.2" fill="#000" d="M20.201,5.169c-8.254,0-14.946,6.692-14.946,14.946c0,8.255,6.692,14.946,14.946,14.946 s14.946-6.691,14.946-14.946C35.146,11.861,28.455,5.169,20.201,5.169z M20.201,31.749c-6.425,0-11.634-5.208-11.634-11.634 c0-6.425,5.209-11.634,11.634-11.634c6.425,0,11.633,5.209,11.633,11.634C31.834,26.541,26.626,31.749,20.201,31.749z"></path><path fill="#000" d="M26.013,10.047l1.654-2.866c-2.198-1.272-4.743-2.012-7.466-2.012h0v3.312h0 C22.32,8.481,24.301,9.057,26.013,10.047z" transform="rotate(98.3774 20 20)"><animatetransform attributetype="xml" attributename="transform" type="rotate" from="0 20 20" to="360 20 20" dur="0.5s" repeatcount="indefinite"></animatetransform></path></svg>
      </div>
      <!-- Loader -->

      <div class="bgback">
      </div>

      <!-- Animated Sparkles -->
      <div class="th-sparkles">
      </div>
      <!--/ Animated Sparkles -->

      <!-- iOS Slider wrapper with animateme scroll efect -->
      <div class="iosSlider kl-slideshow-inner animateme" data-trans="6000" data-autoplay="1" data-infinite="true" data-when="span" data-from="0" data-to="0.75" data-translatey="300" data-easing="linear">
        <!-- Slides -->
        <div class="kl-iosslider hideControls">

          <?php if ($post -> h_slider): ?>
            <?php foreach ($post -> h_slider as $key => $slide): ?>
                  <!-- Slide 1 -->
                  <div class="item iosslider__item">

                    <?php if (isset($slide['text']) && $slide['text'] != ""): ?>
                        <div class="kl-video-container">
                          <!-- Video wrapper -->
                          <div class="kl-video-wrapper video-grid-overlay">
                            <!-- Self Hosted Video Source -->
                            <div class="kl-video valign halign" style="width: 100%; height: 100%;" data-setup='{ 
                              "position": "absolute", 
                              "loop": true, 
                              "autoplay": true, 
                              "muted": true, 
                              "youtube": "<?=$slide['text'];?>", 
                              "poster": "<?=$slide['image'];?>", 
                              "video_ratio": "1.7778" }'>
                            </div>
                            <!--/ Self Hosted Video Source -->
                          </div>
                          <!--/ Video wrapper -->
                        </div>
                        <!--/ Video background container -->
                    <?php endif ?>
                    <!-- Image -->
                    <div class="slide-item-bg" style="background-image: url(<?=$slide['image'];?>);">
                    </div>
                    <!--/ Image -->

                    <!-- Gradient overlay -->
                    <div class="kl-slide-overlay" style="background: rgba(32,55,152,0.4); background: -moz-linear-gradient(left, rgba(32,55,152,0.4) 0%, rgba(17,93,131,0.25) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(32,55,152,0.4)), color-stop(100%,rgba(17,93,131,0.25))); background: -webkit-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -o-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: -ms-linear-gradient(left, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%); background: linear-gradient(to right, rgba(32,55,152,0.4) 0%,rgba(17,93,131,0.25) 100%);">
                    </div>
                    <!--/ Gradient overlay -->

                    <!-- Captions container -->
                    <div class="container kl-iosslide-caption kl-ioscaption--style4 s4ext fromleft klios-alignleft kl-caption-posv-middle">
                      <!-- Captions animateme wrapper -->
                      <div class="animateme" data-when="span" data-from="0" data-to="0.75" data-opacity="0.1" data-easing="linear">
                        <!-- Main Big Title -->
                        <h2 class="main_title has_titlebig">
                          <span>
                            <?=$slide['title'];?>
                          </span>
                        </h2>
                        <!--/ Main Big Title -->

                        <!-- Link more button -->
                        <a class="more" href="<?=$slide['url'];?>" target="_self" title="<?=$slide['title'];?>">
                          სრულად
                        </a>
                        <!--/ Link more button -->
                      </div>
                      <!--/ Captions animateme wrapper -->
                    </div>
                    <!--/ Captions container -->
                  </div>
          <!--/ Slide 1 -->
            <?php endforeach ?>
          <?php endif ?>

        </div>
        <!--/ Slides -->

        <!-- Navigation Controls - Prev -->
        <div class="kl-iosslider-prev">
          <!-- Arrow -->
          <span class="thin-arrows ta__prev"></span>
          <!--/ Arrow -->

          <!-- Label - prev -->
          <div class="btn-label">
            PREV
          </div>
          <!--/ Label - prev -->
        </div>
        <!--/ Navigation Controls - Prev -->

        <!-- Navigation Controls - Next -->
        <div class="kl-iosslider-next">
          <!-- Arrow -->
          <span class="thin-arrows ta__next"></span>
          <!--/ Arrow -->

          <!-- Label - next -->
          <div class="btn-label">
            NEXT
          </div>
          <!--/ Label - next -->
        </div>
        <!--/ Navigation Controls - Prev -->
      </div>
      <!--/ iOS Slider wrapper with animateme scroll efect -->


      <div class="scrollbarContainer">
      </div>

      <!-- Bottom mask style 2 -->
      <div class="kl-bottommask kl-bottommask--shadow_ud">
      </div>
      <!--/ Bottom mask style 2 -->
    </div>
    <!--/ Slideshow - iOS Slider element with animateme scroll efect, custom height and bottom mask style 2 -->
