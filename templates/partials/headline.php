<?php 
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
?>
<div id="page_header" class="page-subheader site-subheader-cst">
    <div class="bgback"></div> 

    <!-- Animated Sparkles -->
    <div class="th-sparkles"></div>
    <!--/ Animated Sparkles -->


    <?php if (strpos($image[0], 'media/default.png')  == false): ?>
    <!-- Background -->
    <div class="kl-bg-source">
        <!-- Background image -->
        <div class="kl-bg-source__bgimage" style="background-image: url(<?=$image[0];?>); background-repeat: no-repeat; background-attachment: scroll; background-position-x: center; background-position-y: center; background-size: cover;"></div>
        <!--/ Background image -->
    </div>
    <!--/ Background -->
    <?php endif ?>

    <!-- Sub-Header content wrapper -->
    <div class="ph-content-wrap d-flex">
        <div class="container align-self-center">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <!-- Breadcrumbs -->
                    <ul class="breadcrumbs fixclear">
                        <li><a href="<?=site_url();?>"><?=__('Home','gg') ?> </a></li>
                        <li><?=$post -> post_title;?></li>
                    </ul>
                    <!--/ Breadcrumbs -->

                    <!-- Current date -->

                    <!--/ Current date -->

                    <div class="clearfix"></div>
                </div>
                <!--/ col-sm-12 col-md-6 col-lg-6 -->

                <div class="col-sm-12 col-md-6 col-lg-6">
                    <!-- Sub-header titles -->
                    <div class="subheader-titles">
                        <!-- <h2 class="subheader-maintitle"><?=$post -> post_title;?></h2> -->
                        <h4 class="subheader-subtitle"><?=$post -> headline;?></h4>
                    </div>
                    <!--/ Sub-header titles -->
                </div>
                <!--/ col-sm-12 col-md-6 col-lg-6 -->
            </div>
            <!--/ row -->
        </div>
        <!--/ .container .align-self-center -->
    </div>
    <!--/ Sub-Header content wrapper .d-flex -->

    <!-- Sub-header Bottom mask style 3 -->
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#ffffff"></path>
        </svg>
        <i class="fas fa-angle-down"></i>
    </div>
    <!--/ Sub-header Bottom mask style 3 -->
</div>