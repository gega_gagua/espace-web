<?php 
  $gallery_options = get_option('gallery_options'); 
  $payment = get_option('payment');
  $details_options = get_option('details_options');
?>
<div class="tabs-wrapper project-tabs">
  <ul class="nav nav-tabs nav-tabs-left" role="tablist">
    <li class="nav-item active">
      <a class="nav-link" data-toggle="tab" href="#about-project" role="tab" aria-selected="true"><?=__('About project', 'gg'); ?> </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#payment-terms" role="tab" aria-selected="false"><?=__('Terms of payment', 'gg'); ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#building-process" role="tab" aria-selected="false"><?=__('Construction process','gg'); ?> </a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#photo-gallery" role="tab" aria-selected="false"><?=__('Photo gallery','gg'); ?></a>
    </li>
  </ul>
  
  <div class="tab-content flex-column flex-1">
    <div class="tab-pane active-flex flex-column min-h-100 fade show active" id="about-project" role="tabpanel">
      <div class="project-about">
        <?=apply_filters('the_content', $details_options['about_project_' . ICL_LANGUAGE_CODE]);?>
      </div>
    </div>
    <div class="tab-pane active-flex flex-column min-h-100 fade" id="payment-terms" role="tabpanel">
      <div class="project-terms">
        <div class="project-terms-logos">
          <?=apply_filters('the_content', $payment['payment_project_' . ICL_LANGUAGE_CODE]);?>
        </div>
      </div>
      <?php if ($payment['sale_percent']) {; ?>
        <div class="project-terms">
          <div class="project-terms-title"><?=__('In cash', 'gg'); ?></div>
          <div class="row project-terms-logos">
            <div class="col-12 col-sm-6 col-lg-3">
              <div class="project-terms-logo"><?=$payment['sale_percent'];?>%  <?=__('Sale', 'gg'); ?></div>
            </div>
          </div>
        </div>
      <?php };?>
    </div>
    <div class="tab-pane active-flex flex-column min-h-100 fade" id="building-process" role="tabpanel">

      <div class="project-video" data-fancybox data-type="iframe" data-src="<?=$gallery_options['youtube_url'];?>?autoplay=1">
        <div class="project-video-img" style="background-image: url(<?=$gallery_options['youtube_img'];?>);"></div>
        <div class="project-video-details">
          <div class="project-video-live">
            <?=__('Live','gg'); ?>
            <!-- Live -->
          </div>
          <div class="project-video-play">
            <i class="i-play"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane active-flex flex-column min-h-100 fade" id="photo-gallery" role="tabpanel">

      <div class="project-gallery">
        <div class="row">
          
          <?php if ($gallery_options['about_project_ge']): ?>
            <?php foreach ($gallery_options['about_project_ge'] as $key => $img): ?>
              <?php 
                $gFull = wp_get_attachment_image_src( $key, 'large' ,true ); 
                $gMedium = wp_get_attachment_image_src( $key, 'medium' ,true ); 
              ?>
              <div class="col-12 col-sm-4 col-lg-3">
                <a data-fancybox="project-gallery" data-type="image" class="fancybox project-gallery-photo" href="<?=$gFull[0];?>" style="background-image: url(<?=$gMedium[0];?>);" title="Biota"></a>
              </div>
            <?php endforeach ?>
          <?php endif ?>
          
        </div>
      </div>
    </div>
  </div>
</div>

<div class="project-map">
  
<div class="map-wrapper">
  <div class="map-title">
    <span><?php echo __('Location', 'gg'); ?></span>
  </div>
  <div class="map-radius">
    <div class="map"></div>
  </div>
</div>
                    