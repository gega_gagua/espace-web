<section class="th-haslayout th-bgdark th-positionrelative brand-bg" style="overflow: hidden; padding: 0px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <figure class="th-videobox">
                    <img src="http://intermedic.propagandahq.ge/wp-content/uploads/2020/10/Screen-Shot-2020-10-30-at-15.33.47.png" alt="image description">
                    <figcaption>
                        <a class="th-btnplay" href="<?=$post -> v_url;?>" data-lightbox="iframe"><i class="fa fa-play"></i></a>
                        <strong><?=__('Start', 'gg') ?></strong>
                    </figcaption>
                </figure>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                
                <div class="th-counter-box-container">

                    <div class="th-counter">
                        <span class="th-countericon">
                            <i class="fa fa-trophy"></i>
                        </span>
                        <div class="th-counterbox">
                            <div class="th-countertitle">
                                <h2 class="headline"><?=__('Years experience', 'gg') ?></h2>
                            </div>
                            <div class="th-count">
                                <h3 data-from="0" countTo data-to="<?=$post -> v_count;?>" data-speed="8000" data-refresh-interval="50"></h3>
                            </div>
                        </div>
                    </div>

                    <div class="th-counter">
                        <div class="th-counter-cover"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>