<div class="menu-overlay"></div>

<div class="site-menu">
    


    <i class="fa fa-times close" aria-hidden="true"></i>

    <div class="menu-container">
        
        <?php 

            $args = [
                'theme_location'  => 'menu-1',
                'menu'            => 'sf-menu header_right',
                'container'       => 'ul',
                'container_class' => 'sf-menu header_right',
                'container_id'    => '',
                'menu_class'      => 'sf-menu header_right',
                'menu_id'         => '',
                'echo'            => false,
                'fallback_cb'     => 'wp_page_menu',
                'items_wrap'      => '<ul id="menu" class="nav">%3$s</ul>',
                'depth'           => 0,
            ];

            echo wp_nav_menu( $args );

        ?>
        

    </div>

    <div class="copyright">
        <?=$options['copyright']; ?> @ <?=date('Y');?>
    </div>

        <div class="socials">
            
        <nav class="social-links">
                
            <?php if ( exists( $options, 'phone' ) && $options['phone'] != '' ): ?>
                <a href="tel:<?php echo $options['phone']; ?>" class="phone">
                    <i class="fa fa-phone"></i> 
                    <span><?php echo $options['phone']; ?></span>
                </a> 
            <?php endif ?>
            
            <?php if ( exists( $options, 'email' ) && $options['email'] != '' ): ?>
                <a href="mailto:<?php echo $options['email']; ?>" class="email">
                    <i class="fa fa-envelope-o"></i> 
                    <span><?php echo $options['email']; ?></span>
                </a>
            <?php endif ?>
            
            <?php if ( exists( $options, 'facebook' ) && $options['facebook'] != '' ): ?>
                <a href="<?php echo $options['facebook']; ?>" target="_blank">
                    <i class="fa fa-facebook-square"></i> 
                    <span><?php echo __( 'Facebook', 'gg' );?></span>
                </a>
            <?php endif ?> 
            
            <?php if ( exists( $options, 'twitter' ) && $options['twitter'] != '' ): ?>
                <a href="<?php echo $options['twitter']; ?>" target="_blank">
                    <i class="fa fa-twitter"></i> 
                    <span><?php echo __( 'Twitter', 'gg' );?></span>
                </a> 
            <?php endif ?>

            <?php if ( exists( $options, 'youtube' ) && $options['youtube'] != '' ): ?>
                <a href="<?php echo $options['youtube']; ?>" target="_blank">
                    <i class="fa fa-youtube"></i> 
                    <span><?php echo __( 'Youtube', 'gg' );?></span>
                </a> 
            <?php endif ?>

            <?php if ( exists( $options, 'linkedin' ) && $options['linkedin'] != '' ): ?>
                <a href="<?php echo $options['linkedin']; ?>" target="_blank">
                    <i class="fa fa-linkedin"></i> 
                    <span><?php echo __( 'Linkedin', 'gg' );?></span>
                </a> 
            <?php endif ?>

        </nav>

    </div>

</div>