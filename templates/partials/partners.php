<?php 
$args = array (
  'post_type'              => array( 'brand' ),
  'post_status'            => array( 'publish' ),
  'post_parent'			   => 0,
  'posts_per_page'         => '-1',
  'order'                  => 'ASC',
  //'offset'                 => $paged,
);

$partners = new WP_Query( $args );

?>

<?php if (count($partners -> posts)): ?>
	<section class="hg_section light-gray brand-bg brands-list">
	  <div class="container">
	    <div class="row">
	      <div class="col-sm-12">
	        <!-- Logo list -->
	          <div class="recent_works3 recentwork_carousel__crsl js-slick list-home-container" data-slick='{
	            "infinite": true,
	            "slidesToShow": 5,
	            "slidesToScroll": 2,
	            "swipeToSlide": true,
	            "autoplay": true,
	            "autoplaySpeed": 1500,
	            "speed": 300,
	           
	            "arrows": false,
	           
	            "responsive": [
	              {
	                "breakpoint": 1199,
	                "settings": {
	                  "slidesToShow": 5
	                }
	              },
	              {
	                "breakpoint": 767,
	                "settings": {
	                  "slidesToShow": 4
	                }
	              },
	              {
	                "breakpoint": 480,
	                "settings": {
	                  "slidesToShow": 1
	                }
	              }
	            ]
	          }'>
				<?php foreach ($partners -> posts as $key => $partner): ?>
					<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $partner -> ID ), 'medium' ,true ); ?>
					<div class="recent-work_carousel-item">
						<li>
							<a target="_blank" href="<?=get_permalink($partner -> ID);?>" title="<?=$partner -> post_title;?>">
								<img src="<?=$image[0];?>" title="<?=$partner -> post_title;?>" alt="<?=$partner -> post_title;?>">
							</a>
						</li>
					</div>
				<?php endforeach ?>
	            
	          </div>

	      </div>
	      <!--/ col-sm-12 -->
	    </div>
	    <!--/ row -->
	  </div>
	  <!--/ container -->
	</section>
<?php endif ?>