<?php 

/* 
* Template Name: Services
*/

get_header();
global $post;

?>


<!-- Services boxes modern style section -->
<section class="hg_section pt-120 pb-80">
    <div class="container pt-50">
    <div class="row" style="justify-content: center;">
    	<div class="col-md-12">
	        <h3 class="global-title">
	            <?=$post -> post_title; ?>
	        </h3>
        </div>
			<?php if ($post -> services_benef): ?>
				<?php foreach ($post -> services_benef as $key => $value): ?>
					<div class="col-sm-12 col-md-12 col-lg-4">
						<!-- Services box element modern style -->
						<div class="services_box services_box--modern sb--hasicon">
							<!-- Service box wrapper -->
							<div class="services_box__inner clearfix">
								<!-- Icon content -->
								<div class="services_box__icon">
									<!-- Icon wrapper -->
									<div class="services_box__icon-inner">
										<!-- Icon = .icon-noun_65754  -->
										<span class="services_box__fonticon icon-noun_65754"></span>
									</div>
									<!--/ Icon wrapper -->
								</div>
								<!--/ Icon content -->

								<!-- Content -->
								<div class="services_box__content">
									<!-- Title -->
									<h4 class="services_box__title">
										<?=$value['car2_title']; ?>
									</h4>
									<!--/ Title -->


									<!-- List wrapper -->
									<div class="services_box__list-wrapper">
										<span class="services_box__list-bg"></span>
										<!-- List -->
										<ul class="services_box__list">
											<?php foreach ($value['car2_list'] as $key => $l): ?>
												<li><span class="services_box__list-text"><?=$l;?> </span></li>
											<?php endforeach ?>

										</ul>
										<!--/ List -->
									</div>
									<!--/ List wrapper -->
								</div>
								<!--/ Content -->
							</div>
							<!--/ Service box wrapper -->
						</div>
						<!--/ Services box element modern style -->
					</div>
					<!--/ col-sm-12 col-md-12 col-lg-4 -->
				<?php endforeach ?>
			<?php endif ?>

		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Services boxes modern style section -->


<?php get_footer() ?>