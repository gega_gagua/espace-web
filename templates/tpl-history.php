<?php 

/* 
* Template Name: History
*/

get_header();
global $post;
?>

<!-- Page Sub-Header -->
<?php include get_template_directory() . '/templates/partials/headline.php'; ?>
<!--/ Page sub-header -->

<!-- Historic section with custom paddings -->
<section class="hg_section pt-80 pb-80">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<!-- Title element -->
				<div class="kl-title-block text-center tbk-symbol-- tbk-icon-pos--after-title">
					<!-- Title with bold style -->
					<?=apply_filters('the_content', $post -> post_content);?>
					<!--/ Subtitle with thin style -->
				</div>
				<!--/ Title element -->

				<!-- Historic timeline element -->
				<div class="timeline_bar">
					<div class="row">
						
						<?php foreach ($post -> home_benef as $key => $value): ?>
							<!-- Timeline box 1 -->
							<div class=" col-sm-12 col-md-6 <?=($key%2==0) ? 'offset-md-6' : '';?>" data-align="<?=($key%2==0) ? 'right' : 'left';?>">
								<!-- Timeline box -->
								<div class="timeline_box brand-bg">
									<!-- Date -->
									<div class="date">
										<?=$value['add2_year'];?>
									</div>
									<!--/ Date -->

									<!-- Title -->
									<h4 class="htitle">
										<?php if ($value['add2_icon']): ?>
											<img style="height: 80px;width: 80px; object-fit: cover;" src="<?=$value['add2_icon'];?>" alt="">
										<?php endif ?>
										<?=$value['add2_title'];?>
									</h4>
									<!--/ Title -->

									<!-- Content -->
									<p>
										<?=$value['add2_desc'];?>
									</p>
									<!--/ Content -->
								</div>
								<!--/ Timeline box -->
							</div>
							<!--/ Timeline box 1 -->							
						<?php endforeach ?>					

						<!-- End timeline -->
						<div class="col-sm-12 end_timeline">
							<span><?=__('PRESENT', 'gg'); ?></span>
						</div>
						<!--/ End timeline -->
					</div>
					<!--/ row -->
				</div>
				<!--/ Historic timeline element -->
			</div>
			<!--/ col-sm-12 col-md-12 -->
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Historic section with custom paddings -->


<?php get_footer(); ?>