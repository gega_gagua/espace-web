<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package GG
 */

get_header(); ?>

<div class="site wrapper-content"> 
    
    <div class="top_site_main" style="color: rgb(255, 255, 255); background-color: rgb(0, 0, 0); background-image: url(&quot;http://travelwp.physcode.com/wp-content/uploads/2016/12/top-heading.jpg&quot;); padding-top: 126px;">
        <div class="banner-wrapper container article_heading">
            <div class="breadcrumbs-wrapper">
                <ul class="phys-breadcrumb">
                    <li>
                        <a href="<?=site_url();?>" class="home">
                            <?=__('Home','gg'); ?>
                        </a>
                    </li>
                    <li><?=__('404 Not Found','gg'); ?></li>
                </ul> 
            </div>
        </div>
    </div>

    <div class="content-area">
        <div class="container">
            <section class="error-404 not-found">
                <header class="page-header">
                    <h1 class="page-title"><?=__('Oops! That page can’t be found','gg') ?>.</h1>
                </header> 

                <div class="page-content">
                    <p><?=__('It looks like nothing was found at this location. Maybe try one of the links below or a search','gg') ?>?</p>
                    <a href="<?=site_url();?>">
                        <?=__('Go Home','gg'); ?>
                    </a>
                </div> 
            </section> 
        </div>
    </div>

</div>
<?php
get_footer();
