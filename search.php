<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package GG
 */

get_header(); ?>

<div class="page relative">
    <div class="page_layout page_margin_top clearfix">
		
		<!-- headline -->
        <?php 
		// $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
		?>
		<div id="page_header" class="page-subheader site-subheader-cst">
		    <div class="bgback"></div> 

		    <!-- Animated Sparkles -->
		    <div class="th-sparkles"></div>
		    <!--/ Animated Sparkles -->


		    <!-- Sub-Header content wrapper -->
		    <div class="ph-content-wrap d-flex">
		        <div class="container align-self-center">
		            <div class="row">
		                <div class="col-sm-12 col-md-6 col-lg-6">
		                    <!-- Breadcrumbs -->
		                    <ul class="breadcrumbs fixclear">
		                        <li><a href="<?=site_url();?>"><?=__('Home','gg') ?> </a></li>
		                        <li>ძებნის შედეგი</li>
		                    </ul>
		                    <!--/ Breadcrumbs -->

		                    <div class="clearfix"></div>
		                </div>
		                <!--/ col-sm-12 col-md-6 col-lg-6 -->
		                <!--/ col-sm-12 col-md-6 col-lg-6 -->
		            </div>
		            <!--/ row -->
		        </div>
		        <!--/ .container .align-self-center -->
		    </div>
		    <!--/ Sub-Header content wrapper .d-flex -->

		    <!-- Sub-header Bottom mask style 3 -->
		    <div class="kl-bottommask kl-bottommask--mask3">
		        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
		            <defs>
		                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
		                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
		                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
		                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
		                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
		                    <feMerge>
		                        <feMergeNode in="SourceGraphic"></feMergeNode>
		                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
		                    </feMerge>
		                </filter>
		            </defs>
		            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#ffffff"></path>
		        </svg>
		        <i class="fas fa-angle-down"></i>
		    </div>
		    <!--/ Sub-header Bottom mask style 3 -->
		</div>
        <!-- headline end -->

		<?php
		if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

		?>

			<div class="search container" style="margin-top: 20px; margin-bottom: 20px; padding-bottom: 20px; border-bottom: 1px solid #349ed2;">
				
				<a href="<?=get_permalink($post -> ID);?>">
					<h4>
						<?php echo $post -> post_title; ?>
					</h4>

					<div class="desc">
						<?php 
							$cont = strip_tags($post -> post_content);
							echo mb_substr($cont, 0, 200);
						?>...
					</div>
				</a>

			</div>

		<?php

			endwhile;

			// the_posts_navigation();

		else : ?> 

			<h3 class="mt-50 container">
				ჩანაწერი ვერ მოიძებნა
			</h3>
		
		<?php
		// 	include get_template_directory() . '/templates/partials/not-found.php';

		endif; ?>

	</div>
</div>

<?php

get_footer();
