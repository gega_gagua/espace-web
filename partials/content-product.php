<?php
    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }

    global $product;

    // Ensure visibility
    if ( empty( $product ) || ! $product->is_visible() ) {
        return;
    }

    $product_image_gallery = explode( ',' , $post -> _product_image_gallery );
    $regular_price = $post -> _regular_price;
    $sale_price = $post -> _sale_price;
    $url = get_permalink( $post -> ID );
    $mainImage = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
    
?>


<!-- modal -->
<div class="modal fade" id="product-<?=$post -> ID;?>">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                
                <h6 class="modal-title pull-left">
                    <?php echo $post -> post_title; ?>
                </h6>

                <button type="button" class="close pull-right" data-dismiss="modal">
                    <i class="fa fa-times"></i>
                </button>

            </div>

            <div class="modal-body text-center">
                
                <div class="modal-body-inside product-inside">
                
                    <div class="col-sm-8">
                        <img style="width: 100%;" src="<?php echo $mainImage[0]; ?>" alt="<?php echo $post -> post_title; ?>">
                    </div> 

                    <div class="col-sm-4 text-left">
                        <?php echo apply_filters('the_content', $post -> post_content); ?>
                        
                        <div class="clearfix"></div>
                        <div class="single-product-addcart-container">

                            <input class="add-cart-input" min="1" max="99" type="number" value="1">

                            <span
                                data-toggle="tooltip" 
                                data-placement="right"
                                title="Please select street"
                            >
                                
                                <a rel="nofollow" href="<?php echo site_url(); ?>?add-to-cart=<?php echo $post -> ID; ?>" 
                                    data-quantity="1" 
                                    data-product_id="<?php echo $post -> ID; ?>" 
                                    data-product_sku=""
                                    class="btn btn-primary button product_type_simple add_to_cart_button ajax_add_to_cart">
                                    <i class="fa fa-cart-arrow-down"></i> 
                                    <?php echo __('add to cart','gega'); ?>
                                    <i class="fa fa-spinner" aria-hidden="true"></i>
                                </a>

                            </span>

                        </div>

                        <div style="margin-top: 30px;">
                            <div class="row">
                                <div class="col-sm-10 popup-desc">
                                    <ul>
                                        <?php if ( 
                                            $post -> ID == 149 || $post -> ID == 148 || $post -> ID == 147 || $post -> ID == 145 
                                            || $post -> ID == 121 || $post -> ID == 119 || $post -> ID == 117 || $post -> ID == 115 
                                        ): ?>
                                            <li>
                                                
                                                <?php if (ICL_LANGUAGE_CODE == 'ka'): ?>
                                                    <span>ცხარე</span>
                                                <?php else: ?>
                                                    <span>Spicy</span>
                                                <?php endif ?>
                                                <input type="checkbox" name="gemo">
                                            </li>
                                        <?php endif ?>
                                    </ul> 
 
                                    <label for="description">
                                        <?php echo __('Description','gega'); ?>
                                    </label>
                                    <textarea name="text" id="description" cols="30" rows="10"></textarea>
                                </div>
                                <div class="col-sm-2">
                                    <div class="socials">       
                                        <a href="<?php echo $url; ?>" class="fb-share">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="clearfix"></div>
                </div>
            
            </div>

        </div>
    </div>
</div>
<!-- modal end -->

<li data-id="product-<?=$post -> ID;?>" <?php post_class('row irow-xs'); ?>>
    
    <div class="p-img-cont">
        <div class="product-img">
            <a class="grouped_elements" rel="group1">
                <i class="fa fa-search-plus" aria-hidden="true"></i>
                <img src="<?php echo $mainImage[0]; ?>" alt="">
            </a>

        </div>
    </div>

    <div class="col-md-12 col-sm-12">

        <h3>
            <?php echo $post -> post_title; ?>

            <span class="price pull-right">
                <?php if ( $sale_price != "" ): ?>
                    
                    <del>
                        <span class="amount">
                            <?php echo $regular_price; ?> ლ
                        </span>
                    </del> 
                    <ins>
                        <span class="amount">
                            <?php echo $sale_price; ?> ლ
                        </span>
                    </ins>

                <?php else: ?>

                    <ins>
                        <span class="amount">
                            <?php echo $regular_price; ?> ლ
                        </span>
                    </ins>

                <?php endif ?>
            </span>
        </h3>

        <div class="single-product-description">
            <?php 
                $content = apply_filters('the_content', $post -> post_content);
                $content = strip_tags($content);
                $content = mb_substr($content, 0, 40);

            ?>
            <?php echo $content; ?>...
        </div>
    
    </div>

</li>

<?php

    // do_action( 'woocommerce_before_shop_loop_item' );

    // do_action( 'woocommerce_before_shop_loop_item_title' );

    // do_action( 'woocommerce_shop_loop_item_title' );

    // do_action( 'woocommerce_after_shop_loop_item_title' );

    // do_action( 'woocommerce_single_add_to_cart' );
?>

