<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GG
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>

    <!-- Google Fonts CSS Stylesheet // More here https://www.google.com/fonts#UsePlace:use/Collection:Open+Sans -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,400,600,600italic,700,800,800italic" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>


        <!-- ***** Boostrap Custom / Addons Stylesheets ***** -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.css" type="text/css" media="all">

    <!-- Font Awesome icons library -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/font-awesome/css/font-awesome.min.css" type="text/css" media="all">

    <!-- Required CSS file for iOS Slider element -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/sliders/ios/style.css" type="text/css" media="all">

    <!-- ***** Main + Responsive & Base sizing CSS Stylesheet ***** -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/template.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/responsive.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/base-sizing.css" type="text/css" media="all">

    <!-- Required custom CSS file for this niche -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/niches/custom-automotive.css" type="text/css" />

    <!-- Custom CSS Stylesheet (where you should add your own css rules) -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/custom.css" type="text/css" />

    <!-- Modernizr Library -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/modernizr.min.js"></script>

    <!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.js"></script>



</head>

<body data-spy="scroll" data-target=".header" data-offset="55" <?php body_class('preloader'); ?>>


    <!-- Page Wrapper -->
    <div id="page_wrapper">
        <!-- Header style 1 -->
        <header id="header" class="site-header cta_button" data-header-style="1">
            <!-- Header background -->
            <div class="kl-header-bg"></div>
            <!--/ Header background -->

            <!-- Header wrapper -->
            <div class="site-header-wrapper">
                <!-- Header Top wrapper -->
                <div class="site-header-top-wrapper">
                    <!-- Header Top container -->
                    <div class="siteheader-container container">
                        <!-- Header Top -->
                        <div class="site-header-row site-header-top d-flex justify-content-between">
                            <!-- Header Top Left Side -->
                            <div class="site-header-top-left d-flex">
                                <!-- Header Top Social links -->
                                <h1 class="site-logo logo" id="logo">
                                    <a href="<?=site_url();?>" title="">
                                        <img src="http://vue.ge/gega/products/espace/wp-content/uploads/2020/12/Group-3.svg" class="logo-img" alt="espace" title="espace" />
                                    </a>
                                </h1>
                                <!--/ Header Top Social links -->

                                <div class="clearfix visible-xxs">
                                </div>

                            </div>
                            <!--/ .site-header-top-left -->

                            <!-- Header Top Right Side -->
                            <div class="site-header-top-right d-flex">

                                <div class="main-menu-wrapper">
                                    <!-- Responsive menu trigger -->
                                    <div id="zn-res-menuwrapper">
                                        <a href="#" class="zn-res-trigger "></a>
                                    </div>
                                    <!--/ responsive menu trigger -->

                                    <!-- Main menu -->
                                    <div id="main-menu" class="main-nav zn_mega_wrapper hidden-xs">
                                        <?php 

                                              $args = [
                                                  'theme_location'  => 'menu-1',
                                                  'menu'            => '',
                                                  'container'       => '',
                                                  'container_class' => '',
                                                  'container_id'    => '',
                                                  'menu_class'      => '',
                                                  'menu_id'         => '',
                                                  'echo'            => false,
                                                  'fallback_cb'     => 'wp_page_menu',
                                                  'items_wrap'      => '<ul id="menu-main-menu" class="main-menu zn_mega_menu">%3$s</ul>',
                                                  'depth'           => 0,
                                              ];

                                              echo wp_nav_menu( $args );

                                          ?>
                                    </div>
                                    <!--/ Main menu -->
                                </div>
                                

                                 <div class="site-header-main-right d-flex justify-content-end align-items-center">
                                    
                                     <!-- header search -->
                                    <div id="search" class="header-search align-self-center">
                                      <a href="#" class="searchBtn "><span class="fas fa-search white-icon"></span></a>
                                      <div class="search-container">
                                        <form id="searchform" role="search" class="header-searchform" action="<?php echo home_url( '/' ); ?>" method="get" >
                                          <input id="s" name="s" maxlength="20" class="inputbox" type="text" size="20" value="SEARCH ..." onblur="if (this.value=='') this.value='SEARCH ...';" onfocus="if (this.value=='SEARCH ...') this.value='';">
                                          <button type="submit" id="searchsubmit" class="searchsubmit fas fa-search white-icon"></button>
                                        </form>
                                      </div>
                                    </div>
                                    <!--/ header search -->
                                </div>

                                <!-- Languages -->
                                <?php 
                                    $langs = icl_get_languages('skip_missing=0&orderby=KEY&order=DIR&link_empty_to=str'); 
                                ?>
                                <div class="languages">
                                    <?php foreach ($langs as $key => $l): ?>
                                        <li class="toplang-item">
                                          <a class="<?=($l['active']) ? 'active' : '' ?>" href="<?=$l['url']; ?>">
                                             <span> 
                                            <?=$l['code']; ?> </span>
                                          </a>
                                        </li>
                                    <?php endforeach ?>
                                </div>
                                <!--/ Languages -->

                            </div>
                            <!--/ .site-header-top-right -->
                        </div>
                        <!--/ .site-header-row .site-header-top -->
                    <!--/ .siteheader-container .container -->
                </div>
                <!--/ Header Top wrapper -->

            </div>
            <!--/ Header wrapper -->
        </header>
        <!-- / Header style 1 -->
