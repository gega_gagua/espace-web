<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package GG
 */

$options = get_option( 'general_options' );

?>
  
    <!-- Footer - Default Style 1 -->
    <footer id="footer" data-footer-style="1">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-5 mb-30">
            <!-- Title -->
            <h3 class="title m_title">
              Navigation
            </h3>

            <div class="sbs">
              <ul class="menu">
                <li><a href="about-us.html">About us</a></li>
                <li><a href="our-team.html">Our team</a></li>
                <li><a href="our-team.html">Our team</a></li>
                <li><a href="our-team.html">Our team</a></li>
                <li><a href="our-team.html">Our team</a></li>
              </ul>
            </div>
          </div>
          <!--/ col-sm-12 col-md-5 mb-30 -->
          <!-- col-sm-12 col-md-4 mb-30 -->

          <div class="col-sm-12 col-md-4 mb-30">
            <!-- Title -->
            <h3 class="title m_title">
              GET IN TOUCH
            </h3>

            <!-- Contact details -->
            <div class="">
              <p>
                <strong>T  
                  +995 32 243 34 73
                </strong><br>
                Email: <a href="mailto:info@e-space.ge">info@e-space.ge</a>
              </p>
              <ul class="social-icons sc--clean clearfix">
                <li><a href="https://www.facebook.com/espace.ge" target="_blank" class="fab fa-facebook-f" title="Facebook"></a></li>
                <li><a href="https://www.instagram.com/e_space.ge/?hl=en" target="_blank" class="fab fa-instagram" title="Facebook"></a></li>
                <li><a href="https://www.youtube.com/channel/UCfeHTLEnFdvroFTWw0tv9Bg?view_as=subscriber" target="_blank" class="fab fa-youtube" title="Facebook"></a></li>
                <li><a href="https://www.linkedin.com/company/ltd-e-space/" target="_blank" class="fab fa-linkedin-in" title="Facebook"></a></li>
              </ul>

            </div>
            <!--/ .contact-details -->
          </div>
          <!--/ col-sm-12 col-md-3 mb-30 -->

          <div class="col-sm-12 col-md-3 mb-30">

            <h3 class="title m_title">
              მისამართი
            </h3>

            <div class="">
              <p>
                ზურაბ და თეიმურაზ ზალდასტანიშვილის N16, თბილისი
              </p>
            </div>
          </div>
        </div>
        <!--/ row -->



        <div class="row">
          <div class="col-sm-12">
            <div class="bottom clearfix">
              <!-- copyright -->
              <div class="copyright">
                <a href="<?=site_url();?>">
                  <img src="http://vue.ge/gega/products/espace/wp-content/uploads/2020/11/logoooooooooo-eng-PNG-1.png" alt="E-space">
                </a>
                
                <p class="float-right">
                  © <?=date('Y') ?> All rights reserved.
                </p>
              </div>
              <!--/ copyright -->
            </div>
            <!--/ bottom -->
          </div>
          <!--/ col-sm-12 -->
        </div>
        <!--/ row -->
      </div>
      <!--/ container -->
    </footer>
    <!--/ Footer - Default Style 1 -->
  </div>
  <!--/ Page Wrapper -->


  <!-- JS FILES // These should be loaded in every page -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/kl-plugins.js"></script>

  <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="1653854168267854">
      </div>
  <!-- JS FILES // Loaded on this page -->
  <!-- Requried js script for Slideshow Scroll effect (uncomment bellow script to activate) -->
  <!-- <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/plugins/scrollme/jquery.scrollme.js"></script> -->

  <!-- Required js script file for iOS Slider element -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/plugins/_sliders/ios/jquery.iosslider.min.js"></script>

  <!-- Required js trigger file for iOS Slider element -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/trigger/slider/ios/kl-ios-slider.js"></script>

  <!-- Slick required js script file for Recent Work carousel & Partners carousel elements -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/plugins/_sliders/slick/slick.js"></script>

  <!-- Required js trigger file for Recent Work carousel & Partners carousel elements -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/trigger/kl-slick-slider.js"></script>

  <!-- Custom Kallyas JS codes -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/kl-scripts.js"></script>

  <!-- Custom user JS codes -->
  <script type="text/javascript" src="<?=get_template_directory_uri();?>/assets/js/kl-custom.js"></script>

<?php wp_footer(); ?>

</body>
</html>
