<?php
/**
 * The template for displaying pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#page
 *
 * @package GG
 */

get_header(); 
global $post;
?>

<!-- Page Sub-Header -->
<?php include get_template_directory() . '/templates/partials/headline.php'; ?>
<!--/ Page sub-header -->


<!-- Title + Services + Skills Diagram section with custom paddings -->
<section class="hg_section pt-80 pb-50">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">

					<!-- Sub-Title with custom size and weight -->
					<div class="post-content">
						<?=apply_filters('the_content', $post -> post_content);?>
					</div>
					<!--/ Sub-Title -->
				</div>

				<!-- separator -->
				<div class="hg_separator clearfix mb-60">
				</div>
				<!--/ separator -->
			</div>
			<!--/ col-sm-12 col-md-12 -->

		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>
<!--/ Title + Services + Skills Diagram section with custom paddings -->


<?php
get_footer();
