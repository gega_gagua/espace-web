<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package GG
 */

get_header(); 
global $post;
$image   = wp_get_attachment_image_src( get_post_thumbnail_id( $post -> ID ), 'large' ,true );
$url     = get_permalink( $post -> ID );
$cat = get_the_category($post -> ID);

$shareCount = 0;
$json = file_get_contents('https://graph.facebook.com/' . get_permalink($post -> ID) );
if ($json) {
	$obj = json_decode($json);
	if ($obj -> share -> comment_count) {
		$shareCount = $obj -> share -> comment_count;
	}
}
$cat = get_the_terms($post -> ID, 'product_cat');
global $product;
$attachment_ids = $product->get_gallery_image_ids();
?>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/ka_GE/sdk.js#xfbml=1&version=v3.2&appId=226615747674441&autoLogAppEvents=1"></script>

<div id="page_header" class="page-subheader min-200 no-bg"></div>

<section id="content" class="hg_section pb-100 kl-store-page brand-bg">
	<div class="container">
		<div class="row">
			<!-- Content with right sidebar -->
			<div class="right_sidebar col-sm-12 col-md-12 col-lg-12 mb-md-30">
				<!-- Product element -->
				<div class="product">
					<!-- Product page -->
					<div class="row product-page">
						<!-- Product main images -->
						<div class="single_product_main_image col-sm-12 col-md-5 col-lg-5 mb-sm-40">
							<!-- Badge container -->
							<?php if ($post -> _sale_price): ?>
								<div class="hg_badge_container">
									<span class="hg_badge_sale">
										Sale! Price <?=$post -> _sale_price;?> ლარი
									</span>
									<!-- <span class="hg_badge_new">NEW!</span> -->
								</div>
							<?php endif ?>
							<!--/ Badge container -->

							<!-- Images -->
							<div class="images">
								<!-- Main image -->
								<a href="<?=$image[0];?>" class="kl-store-main-image zoom" title="<?=$post -> post_title;?>">
									<img style="max-height: 300px;" src="<?=$image[0];?>" class="" alt="<?=$post -> post_title;?>" title="<?=$post -> post_title;?>">
								</a>
								<!-- Main image -->

								<!-- Thumbnails -->
								<div class="thumbnails columns-4">
									<!-- Thumb #1 -->
									<?php foreach ($attachment_ids as $attachment_id): ?>
										<?php 
											$fimage   = wp_get_attachment_image_src( $attachment_id, 'full' ,true );
											$simage   = wp_get_attachment_image_src( $attachment_id, 'large' ,true );
										?>
										<a href="<?=$fimage[0];?>" class="zoom first text-center" title="<?=$post -> post_title;?>">
											<!-- Image -->
											<img style="height: 80px; object-fit: cover;" src="<?=$simage[0];?>" class="" alt="<?=$post -> post_title;?>" title="<?=$post -> post_title;?>">
										</a>
									<?php endforeach ?>
								</div>
								<!--/ Thumbnails -->
							</div>
							<!--/ Images -->
						</div>
						<!--/ single_product_main_image single_product_main_image col-sm-12 col-md-5 col-lg-5 mb-sm-40 -->

						<!-- Main data -->
						<div class="main-data col-sm-12 col-md-7 col-lg-7">
							<div class="summary entry-summary">
								<!-- Product title -->
								<h2 class="product_title entry-title">
									<?=$post -> post_title;?>
								</h2>

								<!-- Price -->
								<div>
									<?php if ($post -> _sale_price): ?>
										<p class="price">
											<del data-was="WAS">
												<span class="amount"><?=$post -> _regular_price;?> ლარი</span>
											</del>
											<ins data-now="NOW">
												<span class="amount"><?=$post -> _sale_price;?> ლარი</span>
											</ins>
										</p>
									<?php else: ?>
										<p class="price">
											<ins data-now="NOW">
												<span class="amount"><?=$post -> _regular_price;?> ლარი</span>
											</ins>
										</p>
									<?php endif ?>
									
								</div>
								<!-- Price -->

								<!-- Description -->
								<div>
									<p class="desc kw-details-desc">
										<?php echo wpautop($post -> post_excerpt); ?>
									</p>
								</div>
								<!--/ Description -->

								<!-- Meta -->
								<div class="product_meta">
									<span class="sku_wrapper">
										SKU:
										<?php if ($post -> _sku): ?>
											<span class="sku"><?=$post -> _sku;?></span>
										<?php else: ?>
											<span class="sku">N/A</span> 
										<?php endif ?> 
									</span>
									<span class="posted_in">კატეგორია: 
										<?php foreach ($cat as $key => $c): ?>
											<a href="product-category.html" rel="tag"><?=$c -> name;?></a> 
										<?php endforeach ?>
									</span>

									<div class="mt-20 mb-15">
										<form class="cart" action="">
											<input type="hidden" name="product_id" value="<?php echo $post -> ID; ?>">
											<input type="hidden" name="quantity" value="1">
											<a rel="nofollow" href="http://vue.ge/gega/products/espace/?add-to-cart=<?php echo $post -> ID; ?>" 
			                                    data-quantity="1" 
			                                    data-product_id="<?php echo $post -> ID; ?>" 
			                                    data-product_sku=""
			                                    class="btn btn-primary button  single_add_to_cart_button">
			                                    <i class="fa fa-cart-arrow-down"></i> 
			                                    <?php echo __('add to cart','gega'); ?>
			                                    <!-- <i class="fa fa-spinner" aria-hidden="true"></i> -->
			                                </a>
		                                </form>
	                                </div>

									<div style="margin-top: 30px;" class="fb-share-button" 
						data-href="<?=get_permalink($post -> ID);?>" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=get_permalink($post -> ID);?>&src=sdkpreparse" class="fb-xfbml-parse-ignore">გაზიარება</a></div>
								</div>
								<!--/ Meta -->
							</div>
							<!-- .summary -->
						</div>
						<!--/ main-data col-sm-12 col-md-7 col-lg-7 -->
					</div>
					<!--/ row product-page -->

					<!-- Description & Reviews tabs -->
					<div class="tabbable">
						<!-- Navigation -->
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a href="#tab-description" class="nav-link active" data-toggle="tab">
									აღწერა
								</a>
							</li>
							<li class="nav-item">
								<a href="#tab-reviews" class="nav-link" data-toggle="tab">
									მიმოხილვა (<?=$shareCount;?>)
								</a>
							</li>
						</ul>

						<!-- Tab content -->
						<div class="tab-content">
							<!-- Description -->
							<div class="tab-pane fade show active" id="tab-description" role="tabpanel">
								<h2 class="fs-s mb-15">
									პროდუქტის აღწერა
								</h2>
								<p>
									<?php echo wpautop($post -> post_content); ?>
								</p>
							</div>
							<!--/ Description -->

							<!-- Reviews -->
							<div class="tab-pane fade" id="tab-reviews" role="tabpanel">
								<div id="reviews">
									<div id="comments">
										<h2 class="fs-s mb-15">
											<?=$shareCount;?> მიმოხილვა <?=$post -> post_title;?>
										</h2>
										<div>
											<div class="fb-comments" 
												data-href="<?=get_permalink($post -> ID);?>" data-numposts="5"></div>
										</div>
									</div>

									<div class="clear">
									</div>
								</div>
							</div>
							<!--/ Reviews -->
						</div>
						<!--/ Tab content -->
					</div>
					<!-- Description & Reviews tabs -->

					<!-- Related products -->
					<?php if (count($cat)): ?>
						<?php 
							$relatedPosts = [];
							foreach ($cat as $key => $c) {
								$args = array(
									'post_type' => 'product',
									'post__not_in' => array($post -> ID),
									'tax_query' => array(
								    	array(
										    'taxonomy' => 'product_cat',
										    'field' => 'term_id',
										    'terms' => $c -> term_id
								     	)
								  	)
								);
								$query = new WP_Query( $args );
								foreach ($query -> posts as $key => $value) {
									$relatedPosts[] = $value;
								}
							}
						?>
						<?php if ($relatedPosts): ?>
							<div class="related products">
								<h2>
									<?=__('Related Products', 'gg') ?>
								</h2>

								<!-- Products -->
								<ul class="products">
									<?php foreach ($relatedPosts as $key => $rp): ?>
										<?php 
											$rimage   = wp_get_attachment_image_src( get_post_thumbnail_id( $rp -> ID ), 'large' ,true );
											$rurl     = get_permalink( $rp -> ID );
										?>
										<li class="product">
											<div class="product-list-item prod-layout-classic">

												<!-- Product container link -->
												<a href="<?=$rurl;?>">
													<!-- Image wrapper -->
													<span class="image kw-prodimage">
														<!-- Primary image -->
														<img style="height: 200px;    object-fit: cover;" class="kw-prodimage-img" src="<?=$rimage[0];?>" alt="<?=$rp -> post_title;?>" title="<?=$rp -> post_title;?>">
													</span>
													<!--/ Image wrapper -->

													<!-- Details -->
													<div class="details kw-details fixclear">
														<!-- Title -->
														<h3 class="kw-details-title">
															<?=$rp -> post_title;?>
														</h3>

														<!-- Price -->
														<span class="price">
															<span class="amount">
																<?=$rp -> _regular_price;?> GEL
															</span>
														</span>
														<!--/ Price -->

														<!-- Star rating -->
														<div class="star-rating" title="Rated 5 out of 5">
															<span style="width:100%">
																<strong class="rating">5</strong> out of 5
															</span>
														</div>
													</div>
													<!--/ details fixclear -->
												</a>
												<!-- Product container link -->

												<!-- Actions -->
												<div class="actions kw-actions">
													<a href="#" rel="nofollow" data-product_id="" data-product_sku="" data-quantity="1">
														<svg xmlns="https://www.w3.org/2000/svg" width="28" height="32" viewBox="0 0 28 32">
															<path class="svg-cart-icon" d="M26,8.91A1,1,0,0,0,25,8H20V6A6,6,0,1,0,8,6V8H3A1,1,0,0,0,2,8.91l-2,22A1,1,0,0,0,1,32H27a1,1,0,0,0,1-1.089ZM10,6a4,4,0,0,1,8,0V8H10V6ZM2.1,30L3.913,10H8v2.277a2,2,0,1,0,2,0V10h8v2.277a2,2,0,1,0,2,0V10h4.087L25.9,30H2.1Z"></path>
														</svg>
													</a>
													<a href="<?=$rurl;?>">
														<span class="more-icon fas fa-compress"></span>
													</a>
												</div>
												<!--/ Actions -->
											</div>
											<!--/ product-list-item -->
										</li>
									<?php endforeach ?>
								</ul>
								<!--/ Products -->
							</div>
						<!--/ Related products -->
						<?php endif ?>
					<?php endif ?>
				</div>
				<!--/ Product element -->
			</div>
			<!--/ Content with right sidebar col-sm-12 col-md-12 col-lg-9 mb-md-30 -->		
		</div>
		<!--/ row -->
	</div>
	<!--/ container -->
</section>

<?php
// $post_view_count = $post -> post_view_count + 1;
// update_post_meta( $post -> ID, 'post_view_count', $post_view_count );
get_footer();
