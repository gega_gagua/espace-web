<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package GG
 */

get_header();
global $post;
$dataID = get_the_ID();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$args = array (
  'post_type'              => array( 'post' ),
  'post_status'            => array( 'publish' ),
  'pagination'             => true,
  'paged'                  => $paged,
  'posts_per_page'         => '3',
  'order'                  => 'random',
  //'offset'                 => $paged,
);

$news = new WP_Query( $args );

$args2 = array (
  'post_type'              => array( 'post' ),
  'post_status'            => array( 'publish' ),
);

// The Query
$all_post = new WP_Query( $args2 );
$allPostCount = $all_post->post_count;
$allPostCount = ceil($allPostCount / 6);

$attachment_ids = $post -> images;
$big = $post;
?>

<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/ka_GE/sdk.js#xfbml=1&version=v3.2&appId=226615747674441&autoLogAppEvents=1"></script>

<!-- Latest news page content with white background color and custom paddings -->
<section class="hg_section bg-white pt-80 mt-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="stg-negative-right stg-negative-left pb-40  pr-sm-0 clearfix">
                    <div class="row">

                        <?php 
                            $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $big -> ID ), 'large' ,true );
                            // $content = mb_substr($content, 0, 200);
                            $cat = get_the_category($big -> ID);
                        ?>
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="bpost bp-widget mb-60 kl-store-page">
                                <!-- Header -->
                                <div class="post-head product single_product_main_image">
                                	<h1 style="color: #000;" data-role="title" class="fs-l fw-bold global-title">
                                            <?=$big -> post_title;?>
                                    </h1>
                                    <!-- slider start -->
                                    <div class="images mt-50">
										<!-- Main image -->
											<img style="width: 100%; border-radius: 15px;" src="<?=$bimage[0];?>" class="" alt="<?=$post -> post_title;?>" title="<?=$post -> post_title;?>">
										<!-- Main image -->

										<!-- Thumbnails -->
										<?php if ($attachment_ids && count($attachment_ids)): ?>
											<div class="thumbnails columns-4">
												<!-- Thumb #1 -->
												<?php $k = 0; ?>
												<?php foreach ($attachment_ids as $imgId => $attachment_id): ?>
													<?php if ($k < 5): ?>
														<?php 
															$fimage   = wp_get_attachment_image_src( $imgId, 'full' ,true );
															$simage   = wp_get_attachment_image_src( $imgId, 'large' ,true );
															$k++;
														?>
														<a href="<?=$fimage[0];?>" class="zoom first text-center" title="<?=$post -> post_title;?>">
															<!-- Image -->
															<img style="height: 100px;" src="<?=$simage[0];?>" class="" alt="<?=$post -> post_title;?>" title="<?=$post -> post_title;?>">
														</a>
													<?php endif ?>
												<?php endforeach ?>
											</div>
										<?php endif ?>
										<!--/ Thumbnails -->
									</div>
                                    <!-- slider end -->

                                    <!-- Title -->
                                </div>
                                <!--/ .post-head -->

                                <!-- Content -->
                                <div class="post-content mt-30" style="    color: #231f20;">
                                    <p>
                                        <?=apply_filters('the_content', $big -> post_content);?>
                                    </p>
                                    <div class="comment-form-wrapper">
										<div class="fb-share-button" 
											data-href="<?=get_permalink($post -> ID);?>" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=get_permalink($post -> ID);?>&src=sdkpreparse" class="fb-xfbml-parse-ignore">გაზიარება</a>
										</div>
										<div></div>
										<div class="fb-comments" data-href="<?=get_permalink($post -> ID);?>" data-numposts="5"></div>
									</div>
                                </div>

                                <!-- Details -->
                                <div class="post-details">
                                   
                                </div>
                            <!-- end postcol -->
                        	</div>
                        </div>

            			<div class="latest_posts default-style row">
            				<div class="col-md-12">
	            				<h2 style="color: #000;" data-role="title" class="fs-m fw-bold global-title">
	                                მსგავსი სიახლეები
	                            </h2>
                            </div>	
	                        <!--/ col-sm-12 col-md-12 col-lg-12 -->
	                        <?php foreach ($news -> posts as $keynews => $n): ?>
	                            <?php 
	                                $keynews = $keynews + 1; 
	                                $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $n -> ID ), 'large' ,true );
	                                $content = strip_tags($n -> post_content);
	                                $content = mb_substr($content, 0, 200);
	                                $cat = get_the_category($n -> ID);
	                            ?>
	                          
	                                <div class="col-sm-12 col-md-6 col-lg-4 post">
						                <!-- Post link wrapper -->
						                <a href="<?=get_permalink($n -> ID);?>" class="hoverBorder plus">
						                  <!-- Border wrapper -->
						                  <span class="hoverBorderWrapper">
						                    <!-- Image -->
						                    <img style="height: 230px; object-fit: cover;" src="<?=$bimage[0];?>" class="img-fluid" width="370" height="200" alt="" title="" />
						                    <!--/ Image -->

						                    <!-- Hover border/shadow -->
						                    <span class="theHoverBorder"></span>
						                    <!--/ Hover border/shadow -->
						                  </span>
						                  <!--/ Border wrapper -->

						                  <!-- Button -->
						                  <h6>
						                    <?=__('Read more', 'gg') ?> +
						                  </h6>
						                  <!--/ Button -->
						                </a>
						                <!--/ Post link wrapper -->


						                <!-- Title with link -->
						                <h3 class="m_title">
						                  <a style="font-style: normal;" href="<?=get_permalink($n -> ID);?>">
						                    <?=$n -> post_title;?>
						                  </a>
						                </h3>
						                <!--/ Title with link -->
						              </div>
	           					
		                            <?php if ($keynews%3 == 0): ?>
		                                <div class="clearfix"></div>
		                            <?php endif ?>
	                        <?php endforeach ?>
	                        <!--/ col-sm-12 col-md-6 col-lg-6 -->
						</div>
                    </div>
                    <!--/ row -->
                </div>
                <!-- end .blog-posts -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-8 -->


        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>

<?php
get_footer();
