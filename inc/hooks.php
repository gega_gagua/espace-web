<?php 

function p( $data = array() )
{
    echo '<pre>';
        print_r($data);
    echo '</pre>';
}


// disable toolbar for subscribers
add_action('after_setup_theme', 'remove_admin_bar');

    function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
    }
}




/*
*
* Update post hook
* Update same mail post fields
*
*/
add_action( 'profile_update', 'my_save_post_function', 10, 3 );

function my_save_post_function( $user_id, $old_user_data ) {
 
    $user_email = get_userdata( $user_id ) -> data -> user_email;

    if ( c2c_has_multiple_accounts( $user_email ) ) {
       
        $cats = get_user_meta( $user_id , 'user_category_group', 1);

        $users = c2c_get_users_by_email( $user_email );
        if ( !empty( $users ) ) {
            
            foreach ($users as $key => $user) {
             
                 update_user_meta( $user -> ID , 'user_category_group', $cats );

            }

        }

    }

}


/*
*
* Hide / Show Post 
*
*/
add_action("wp_ajax_nopriv_showHide", "showHide");
add_action("wp_ajax_showHide", "showHide");

function showHide()
{


    add_user_meta( $_POST['userID'], $_POST['status'], $_POST['postId'] );
    exit();

}



/*
*
*  Sentimente
*
*/
add_action("wp_ajax_nopriv_sentiment", "sentiment");
add_action("wp_ajax_sentiment", "sentiment");

function sentiment()
{

    global $wpdb;

    p( $_POST );
    // update post data
    $allSents = get_user_meta( $_POST['userID'], 'sentiments' );
    
    foreach ( $allSents as $key => $userSentiment ) {
        
        if ( strpos( $userSentiment, $_POST['postId'] ) !== false ) {
            
            $wpdb->query( 
                $wpdb->prepare( 
                    "
                        DELETE  FROM $wpdb->usermeta
                                WHERE user_id = %d
                                AND meta_value = %s
                    ",
                        $_POST['userID'], $userSentiment 
                    )
            );
            // update_user_meta( $_POST['userID'], 'sentiments' , $_POST['status'] . '--' . $_POST['postId'] );

        }

    }

    add_user_meta( $_POST['userID'], 'sentiments' , $_POST['status'] . '--' . $_POST['postId'] );



    // update user data
    $data = get_post_meta( $_POST['postId'], 'sentiments', 1 );
    $data = $data . '<p><li> ' . $_POST["userName"] . ' -- ' . $_POST["status"] . ' -- ' . date("Y-m-d h:i") . ' </li></p>';

    update_post_meta( $_POST['postId'], 'sentiments', $data );

    exit();

}



/**
 *
 *  Exists
 *
 *  @param  mixed $obj
 *  @param  array $key
 *  @param  array $result
 *
 *  @return object
 *
 */
function exists($obj, $key, $result = '')
{

    if(is_array($obj))
    {

        $result = array_key_exists($key, $obj) ? $obj[$key] : $result;

    }

    if(is_object($obj))
    {

        $result = property_exists($obj, $key) ? $obj -> {$key} : $result;

    }

    return $result;

}



// retrieves the attachment ID from the file URL
function pippin_get_image_id($image_url) {
    global $wpdb;
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url )); 
        return $attachment[0]; 
}




/**
 *
 *  Gravity form hook on send
 *
 *  @param  mixed $obj
 *  @param  array $key
 *  @param  array $result
 *
 *  @return object
 *
 */
add_action( 'gform_after_submission', 'set_post_content', 10, 2 );
function set_post_content( $entry, $form ) {
 
    if ( $entry['form_id'] == 2 || $entry['form_id'] == 3 ) {
        
        $name = $entry['1'];
        $mail = $entry['3'];
        $image = $entry['5'];
        $text = $entry['4'];
        $postID = get_the_ID();

        $commentdata = array(
            'comment_post_ID' => $postID,
            'comment_author' => $name, 
            'comment_author_email' => $mail,
            'comment_author_url' => '', //fixed value - can be dynamic 
            'comment_content' => strip_tags( $text ), 
            'comment_type' => '',
            'comment_parent' => 0,
        );
        
        $arr = explode('|:||:||:||:|', $image);
        $image_id = $arr[1];
       

        //Insert new comment and get the comment ID
        $comment_id = wp_new_comment( $commentdata );
        add_comment_meta( $comment_id, 'attachmentId', $image_id );

    }

}