<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package GG
 */

get_header();
global $post;
$dataID = get_the_ID();

?>

<div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/ka_GE/sdk.js#xfbml=1&version=v3.2&appId=226615747674441&autoLogAppEvents=1"></script>

<!-- Latest news page content with white background color and custom paddings -->
<section class="hg_section bg-white pt-80 mt-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="stg-negative-right stg-negative-left pb-40 pr-30 pr-sm-0 clearfix">
                    <div class="row">

                        <?php 
                            $bimage = wp_get_attachment_image_src( get_post_thumbnail_id( $big -> ID ), 'full' ,true );
                        ?>
                        <div class="col-sm-12 col-md-12">
                            <div class="bpost bp-widget mb-60 kl-store-page">
                                <!-- Header -->
                                <div class="post-head product single_product_main_image">
                                	<div style="color: #8e83bd;
											    font-weight: bold;
											    font-size: 23px;
											    height: 26px;
											    margin-bottom: 25px;
											    display: block;">
                                		<?=$post -> date; ?>
                                	</div>
                                    <!-- slider start -->
                                    <div class="images">
										<!-- Main image -->
										<a href="<?=$bimage[0];?>" class="kl-store-main-image zoom" title="<?=$post -> post_title;?>">
											<img style="width: 100%" src="<?=$bimage[0];?>" class="" alt="<?=$post -> post_title;?>" title="<?=$post -> post_title;?>">
										</a>
										<!-- Main image -->
									</div>
                                    <!-- slider end -->
                                    <!-- Title -->
                                    <h2 data-role="title" class="fs-m fw-bold"  style="color: #8e83bd; margin-top: 25px; font-size: 33px;">
                                        <?=$post -> post_title;?>
                                    </h2>
                                </div>
                                <!--/ .post-head -->

                                <!-- Content -->
                                <div class="post-content">
                                    <p>
                                        <?=apply_filters('the_content', $post -> post_content);?>
                                    </p>
                                    <div class="comment-form-wrapper">
										<div class="fb-share-button" 
											data-href="<?=get_permalink($post -> ID);?>" data-layout="button_count" data-size="large" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?=get_permalink($post -> ID);?>&src=sdkpreparse" class="fb-xfbml-parse-ignore">გაზიარება</a>
										</div>
										<div></div>
										<div class="fb-comments" data-href="<?=get_permalink($post -> ID);?>" data-numposts="5"></div>
									</div>
                                </div>

                                <!-- Details -->
                                <div class="post-details">
                                   
                                </div>
                            <!-- end postcol -->
                        	</div>
                        </div>
                    </div>
                    <!--/ row -->
                </div>
                <!-- end .blog-posts -->
            </div>
            <!--/ col-sm-12 col-md-12 col-lg-8 -->


        </div>
        <!--/ row -->
    </div>
    <!--/ container -->
</section>

<?php
get_footer();
